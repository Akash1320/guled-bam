package com.bam.issaguled;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.bam.issaguled.other.API_LIST.add_donation_with_login;

public class DonateActivity extends AppCompatActivity implements ProgressClickListener {

    TextView Back;
    Button pay;
    RelativeLayout toolbar;
    TextView title, expired_date;
    EditText amount, card_holder, card_number, zip_code;
    private static final String TAG = "MainActivity";
    TextInputEditText Date;
    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    TextInputLayout nameLayout, amountLayout, zipLayout, dateLayout, numberLayout;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private CardMultilineWidget card_input_widget;
    private Card card;
    public static final int STRIPE_TOKEN_CODE = 212;
    private int START_ALIPAY_REQUEST = 100;
    public static final String STRIPE_TOKEN_ID = "token_id";
   // public static final String PUBLISHABLE_KEY = "pk_test_I71hW1HMRNeKcsF2IRuQk3ga00ZtdU01e5";
    public static final String PUBLISHABLE_KEY = "pk_live_nGIPnvmYzpXAWYyewBFT3rca00IWCuUYIv";
    private String event_id;
    private String token_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);
        myStateview = new MyStateview(this, null);
        myPreferences = new MyPreferences(this);

        if (getIntent() != null) {
            event_id = getIntent().getStringExtra("event_id");
        }
        card_input_widget = findViewById(R.id.card_input_widget);
        Back = (TextView) findViewById(R.id.home);
        title = (TextView) findViewById(R.id.title);
        pay = (Button) findViewById(R.id.payment);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        amount = (EditText) findViewById(R.id.amount);
        card_holder = (EditText) findViewById(R.id.holder);
        card_number = (EditText) findViewById(R.id.card_number);
        expired_date = (TextView) findViewById(R.id.expiry_date);
        zip_code = (EditText) findViewById(R.id.zip);
        numberLayout = (TextInputLayout) findViewById(R.id.card_numberLyout);
        nameLayout = (TextInputLayout) findViewById(R.id.nameLyout);
        amountLayout = (TextInputLayout) findViewById(R.id.amountLyout);
        zipLayout = (TextInputLayout) findViewById(R.id.ziplayout);
        dateLayout = (TextInputLayout) findViewById(R.id.datelayout);
        amount.setText("5");
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Methods.isOnline(DonateActivity.this)) {
                    if (isvalidationAccount()) {
                        card = new Card(
                                card_input_widget.getCard().getNumber(), //card number
                                card_input_widget.getCard().getExpMonth(), //expMonth
                                card_input_widget.getCard().getExpYear(),//expYear
                                card_input_widget.getCard().getCVC()//cvc
                        );
                        buy();
                    }
                }
            }
        });

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DonateActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        expired_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(DonateActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    protected void onBackpressed() {

        Intent intent = new Intent(DonateActivity.this, HomeActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }


    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        expired_date.setText(sdf.format(myCalendar.getTime()));
    }

    private void buy() {
        boolean validation = card.validateCard();
        if (validation) {
            myStateview.showLoading();
            //  ProgressBarDialog.showProgressBar(StripeActivity.this, "");
            new Stripe(DonateActivity.this).createToken(
                    card,
                    PUBLISHABLE_KEY,
                    new TokenCallback() {
                        @Override
                        public void onError(Exception error) {
                            Log.e("Stripe", "------ERROR----" + error.toString());
                            MyToast.display(DonateActivity.this, error.toString());
                            myStateview.showContent();
                        }

                        @Override
                        public void onSuccess(Token token) {
                            myStateview.showContent();
                            Log.e("Stripe", "------SUCESS----" + token.getType());
                            Log.e("Message", "Id-------" + token.getId());
                            Log.e("Message", "Type-------" + token.getType());
                            Log.e("Message", "Account-------" + token.getBankAccount());
                            Log.e("Message", "Card-------" + token.getCard());
                            Log.e("Message", "Created-------" + token.getCreated());
                            Log.e("Message", "Livemode-------" + token.getLivemode());
                            Log.e("Message", "Used-------" + token.getUsed());
                            Log.e("token_id", ">>>>>>>>>>>>>>>>" + token.getId());
                            token_id = token.getId();
                            donate(token.getId());
                        }
                    });

        } else if (!card.validateNumber()) {
            myStateview.showContent();

            Log.e("Stripe", "The card number that you entered is invalid");
        } else if (!card.validateExpiryDate()) {
            myStateview.showContent();

            Log.e("Stripe", "The expiration date that you entered is invalid");
        } else if (!card.validateCVC()) {
            myStateview.showContent();

            Log.e("Stripe", "The CVC code that you entered is invalid");
        } else {
            myStateview.showContent();
            Log.e("Stripe", "The card details that you entered are invalid");
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == START_ALIPAY_REQUEST) {
            if (resultCode == Activity.RESULT_CANCELED) {
                // Do not use the source

            } else {
                // The source was approved.

            }
        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }


    private void donate(String token_id) {
        myStateview.showLoading();
        Log.e("expire_date", ">>>>>>>>>>>>>>>>>>" + card_input_widget.getCard().getExpMonth() + "/" + card_input_widget.getCard().getExpYear());
        Log.e("amount", ">>>>>>>>>>>>>>>>>>" + amount.getText().toString());
        Log.e("card_holder_name", ">>>>>>>>>>>>>>>>>>" + card_holder.getText().toString());
        Log.e("card_number", ">>>>>>>>>>>>>>>>>>" + card_input_widget.getCard().getNumber());
        Log.e("zip_code", ">>>>>>>>>>>>>>>>>>" + zip_code.getText().toString());
        Log.e("login_id", ">>>>>>>>>>>>>>>>>>" + myPreferences.getId());
        Log.e("token_id", ">>>>>>>>>>>>>>>>>>" + token_id);

        AndroidNetworking.post(add_donation_with_login)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("amount", amount.getText().toString())
                .addBodyParameter("card_holder_name", card_holder.getText().toString())
                .addBodyParameter("card_number", card_input_widget.getCard().getNumber())
                .addBodyParameter("expiry_date", card_input_widget.getCard().getExpMonth() + "/" + card_input_widget.getCard().getExpYear())
                .addBodyParameter("zip_code", zip_code.getText().toString())
                .addBodyParameter("login_id", myPreferences.getId())
                .addBodyParameter("event_id", event_id)
                .addBodyParameter("token_id", token_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                MyToast.display(DonateActivity.this, response.getString("message"));
                                finish();
                            } else {
                                myStateview.showContent();
                                MyToast.display(DonateActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private boolean isvalidationAccount() {
        if (amount.getText().toString().isEmpty()) {
            MyToast.display(DonateActivity.this, "Please Enter Amount");
        } else if (amount.getText().toString().equalsIgnoreCase("0")) {
            MyToast.display(DonateActivity.this, "Amount should not be 0");
        } else if (card_holder.getText().toString().trim().length() == 0) {
            Toast.makeText(this, "Please Enter Card Holder Name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (card_input_widget.getCard() == null) {
            Toast.makeText(this, "Please enter card details", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    @Override
    public void onRetryClick() {
        if (Methods.isOnline(this)) {
            donate(token_id);
        }
    }
}
