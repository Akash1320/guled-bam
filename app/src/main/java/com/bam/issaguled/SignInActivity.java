package com.bam.issaguled;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bam.issaguled.Admin.Activity.PasswordActivity;
import com.bam.issaguled.Admin.Activity.responser.SignInResponser;
import com.bam.issaguled.other.GpsTracker;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.easywaylocation.EasyWayLocation;
import com.example.easywaylocation.Listener;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;

import static com.bam.issaguled.other.API_LIST.user_register_social;
import static com.bam.issaguled.other.API_LIST.user_signIN;

public class SignInActivity extends AppCompatActivity implements ProgressClickListener, Listener {
    TextView text_signup;
    TextInputEditText user;
    Button sign_in, admin_login;
    TextInputLayout NameinputLyout;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private Button skip;
    TwitterLoginButton loginButton;
    private static final String TAG = SignInActivity.class.getSimpleName();
    private TwitterLoginButton twitterLoginButton;
    private TwitterAuthClient client;
    private CallbackManager callbackManager;
    LoginButton fbloginButton;
    private FrameLayout ivFb;
    private FrameLayout ivtwitter;
    private String token="";
    private double latitude;
    private double longitude;
    private int PLACE_PICKER_REQUEST = 1;
    private GoogleApiClient mGoogleApiClient;
    private String location="";
    private String[] permissions = new String[]
            {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET,
                    Manifest.permission.INSTALL_LOCATION_PROVIDER,
            };
    private int REQUEST_ID_MULTIPLE_PERMISSIONS = 0;
    private EasyWayLocation easyWayLocation;
    private String retry;

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(SignInActivity.this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
                getLocation();
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(SignInActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        easyWayLocation.startLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        easyWayLocation.endUpdates();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        checkPermissions();
        easyWayLocation = new EasyWayLocation(this, false, this);
        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);
        TwitterCore.getInstance();
        token = FirebaseInstanceId.getInstance().getToken();
        Log.e("device_token", ">>>>>>>>>>>>>>>>>>>>>" + token);
        ivFb = findViewById(R.id.ivFb);
        ivtwitter = findViewById(R.id.ivtwitter);
        client = new TwitterAuthClient();
        //find the id of views
        twitterLoginButton = findViewById(R.id.default_twitter_login_button);
        defaultLoginTwitter();

        text_signup = (TextView) findViewById(R.id.text_signup);
        user = (TextInputEditText) findViewById(R.id.user);
        skip = (Button) findViewById(R.id.skip);
        NameinputLyout = (TextInputLayout) findViewById(R.id.nameinputLyout);
        sign_in = (Button) findViewById(R.id.login);
        admin_login = (Button) findViewById(R.id.admin_login);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                String User = user.getText().toString().trim();
                Methods.hideKeyboardFrom(SignInActivity.this, v);
                if (TextUtils.isEmpty(User)) {
                    MyToast.display(SignInActivity.this, "Please Enter Email Address");
                }
                  else if (!User.matches(emailPattern)) {
                    MyToast.display(SignInActivity.this, "Please Enter Valid Email Address");
                    //  RegEmail.setError("Please enter right email Address");
                    // Toast.makeText(this,"Please enter right email Address",Toast.LENGTH_SHORT).show();
                    return;

                }
                else {
                    if (Methods.isOnline(SignInActivity.this)) {
                        signin();
                    }

                }

            }
        });
        admin_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String User = user.getText().toString().trim();
                if (TextUtils.isEmpty(User)) {
                    MyToast.display(SignInActivity.this, "Please Enter Details");
                    //Toast.makeText(getApplicationContext(),"Please Enter Details",Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    NameinputLyout.setErrorEnabled(false);
                }
            }
        });

        text_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });


        ivFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fbloginButton.performClick();

            }
        });

        ivtwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                twitterLoginButton.performClick();
            }
        });

        fbloginButton = (LoginButton) findViewById(R.id.login_button);
         fbloginButton.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);

        fbloginButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        fbloginButton.setReadPermissions("email", "public_profile");
        callbackManager = CallbackManager.Factory.create();
        fbloginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Log.e("LOGIN_RESULT", "-----------------" + loginResult.getAccessToken());

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {


                                // Application code
                                Log.e("FB_LOGIN_RESPONSE", response.toString());
                                String name = null, first_name = null, last_name = null, email = null, id = null;//, link = null;

                                try {
                                    JSONObject data = response.getJSONObject();
                                    if (data.has("email")) {
                                        email = data.getString("email");

                                    } else {
                                        email = "123";

                                    }

                                    name = data.getString("name");
                                    first_name = data.getString("first_name");
                                    last_name = data.getString("last_name");
                                    id = data.getString("id");
                                    Log.e("name", ">>>>>>>>>>>>>>" + name);
                                    Log.e("first_name", ">>>>>>>>>>>>>>" + first_name);
                                    Log.e("last_name", ">>>>>>>>>>>>>>" + last_name);
                                    Log.e("id", ">>>>>>>>>>>>>>" + id);
                                    Log.e("email", ">>>>>>>>>>>>>>" + email);
                                 //   Toast.makeText(SignInActivity.this, "Facebook Login Success", Toast.LENGTH_SHORT).show();
                                    // validateSignUpFacebook(name, email, "Facebook", "https://graph.facebook.com/" + id + "/picture?type=large");
                                    socialLogin(name, email, "facebook");

                                } catch (JSONException e) {
                                    Log.e("EXCEPTION_OCCURED_1", "-------------" + e.toString());

                                    e.printStackTrace();

                                }
                                //  MyPreferences.getInstance(mActivity).setPROFILEPIC("https://graph.facebook.com/" + id + "/picture?type=large");
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,first_name,last_name,gender, birthday,link");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

                Log.e("CANCELLED", "--------------USER_CANCELLED_THE_ACTION");

            }

            @Override
            public void onError(FacebookException error) {
                // TODO Auto-generated method stub

                Log.e("ERROR_MESSAGE", "--------------" + error.toString());

            }
        });
        getHashkey();
    }

    public void getHashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.e("Base64", Base64.encodeToString(md.digest(), Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("Name not found", e.getMessage(), e);

        } catch (NoSuchAlgorithmException e) {
            Log.d("Error", e.getMessage(), e);
        }
    }

    public void defaultLoginTwitter() {
        //check if user is already authenticated or not
        if (getTwitterSession() == null) {

            //if user is not authenticated start authenticating
            twitterLoginButton.setCallback(new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {

                    // Do something with result, which provides a TwitterSession for making API calls
                    TwitterSession twitterSession = result.data;
                    //call fetch email only when permission is granted
                    fetchTwitterEmail(twitterSession);
                }

                @Override
                public void failure(TwitterException exception) {
                    Log.e("error", ">>>>>>>>>>>>>>>>>>" + exception.getMessage());
                    // Do something on failure
                    Toast.makeText(SignInActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            CookieSyncManager.createInstance(SignInActivity.this);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeSessionCookie();
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
            myPreferences.clearPreferences();
            //if user is already authenticated direct call fetch twitter email api
            Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show();

        }
    }

    public void fetchTwitterEmail(final TwitterSession twitterSession) {
        client.requestEmail(twitterSession, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                //here it will give u only email and rest of other information u can get from TwitterSession

                Log.e("userid", ">>>>>>>>>>>>>" + twitterSession.getUserId());
                Log.e("name", ">>>>>>>>>>>>>" + twitterSession.getUserName());
                Log.e("email", ">>>>>>>>>>>>>" + result.data);
                fetchTwitterImage();
           //     Toast.makeText(SignInActivity.this, "Twitter Login Success", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(SignInActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fetchTwitterImage() {
        //check if user is already authenticated or not
        if (getTwitterSession() != null) {

            //fetch twitter image with other information if user is already authenticated

            //initialize twitter api client
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();

            //Link for Help : https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-verify_credentials

            //pass includeEmail : true if you want to fetch Email as well
            Call<User> call = twitterApiClient.getAccountService().verifyCredentials(true, false, true);
            call.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    User user = result.data;
                    Log.e("userid", ">>>>>>>>>>>>>" + user.id);
                    Log.e("name", ">>>>>>>>>>>>>" + user.name);
                    Log.e("email", ">>>>>>>>>>>>>" + user.email);
                    Log.e("screen  name", ">>>>>>>>>>>>>" + user.screenName);
                    if (user.email.isEmpty()) {
                        socialLogin(user.name, user.screenName, "twitter");
                    } else {
                        socialLogin(user.name, user.email, "twitter");
                    }
                   // Toast.makeText(SignInActivity.this, "Twitter Login Success", Toast.LENGTH_SHORT).show();
                    String imageProfileUrl = user.profileImageUrl;
                    Log.e(TAG, "Data : " + imageProfileUrl);
                    //NOTE : User profile provided by twitter is very small in size i.e 48*48
                    //Link : https://developer.twitter.com/en/docs/accounts-and-users/user-profile-images-and-banners
                    //so if you want to get bigger size image then do the following:
                    imageProfileUrl = imageProfileUrl.replace("_normal", "");

           /*         ///load image using Picasso
                    Picasso.with(TwitterLoginActivity.this)
                            .load(imageProfileUrl)
                            .placeholder(R.mipmap.ic_launcher_round)
                            .into(userProfileImageView);*/
                }
                @Override
                public void failure(TwitterException exception) {
                    Toast.makeText(SignInActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            CookieSyncManager.createInstance(SignInActivity.this);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeSessionCookie();
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
            myPreferences.clearPreferences();
            //if user is not authenticated first ask user to do authentication
            Toast.makeText(this, "First to Twitter auth to Verify Credentials.", Toast.LENGTH_SHORT).show();
        }

    }


    private TwitterSession getTwitterSession() {
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();

        //NOTE : if you want to get token and secret too use uncomment the below code
        /*TwitterAuthToken authToken = session.getAuthToken();
        String token = authToken.token;
        String secret = authToken.secret;*/

        return session;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Pass the activity result to the twitterAuthClient.
        if (client != null)
            client.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    private void signin() {
        myStateview.showLoading();
        Log.e("location", ">>." + location);
        Log.e("deviceToken", ">>." + token);
        Log.e("deviceType", ">>." + "android");
        Log.e("email", ">>." + user.getText().toString());
        AndroidNetworking.post(user_signIN)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addBodyParameter("email", user.getText().toString())
                .addBodyParameter("deviceType", "android")
                .addBodyParameter("deviceToken", token)
                .addBodyParameter("location", location)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        SignInResponser signInResponser = gson.fromJson(response.toString(), SignInResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                myPreferences.setToken(signInResponser.getAccessToken());
                                myPreferences.setId(String.valueOf(signInResponser.getData().getUserId()));
                                myPreferences.setEmail(String.valueOf(signInResponser.getData().getEmail()));
                                myPreferences.setMobile(String.valueOf(signInResponser.getData().getMobileNumber()));
                                myPreferences.setUser_name(String.valueOf(signInResponser.getData().getName()));
                                myPreferences.setCity(String.valueOf(signInResponser.getData().getCityId()));
                                myPreferences.setCountry(String.valueOf(signInResponser.getData().getCountryId()));
                                myPreferences.setLogin("1");
                                myPreferences.setLogin_type("self");
                                Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                                intent.putExtra("event_id", "");
                                startActivity(intent);
                                finishAffinity();
                            } else if (response.getString("success").equalsIgnoreCase("201")) {
                                myStateview.showContent();
                                myPreferences.setEmail(response.getString("email"));
                                Intent intent = new Intent(SignInActivity.this, PasswordActivity.class);
                                startActivity(intent);
                            } else {
                                myStateview.showContent();
                                MyToast.display(SignInActivity.this, "User Not Found");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry = "1";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void socialLogin(String name, String email, String type) {
        myStateview.showLoading();
        Log.e("name", ">>>>>>>>>>>>>>>>>>" + name);
        Log.e("email", ">>>>>>>>>>>>>>>>>>" + email);
        Log.e("register_type", ">>>>>>>>>>>>>>>>>>" + type);
        Log.e("deviceToken", ">>." + token);
        Log.e("deviceType", ">>." + "android");
        Log.e("location", ">>." + location);
        AndroidNetworking.post(user_register_social)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addBodyParameter("name", name)
                .addBodyParameter("email", email)
                .addBodyParameter("register_type", type)
                .addBodyParameter("deviceType", "android")
                .addBodyParameter("deviceToken", token)
                .addBodyParameter("location", location)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        SignInResponser signInResponser = gson.fromJson(response.toString(), SignInResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                myPreferences.setToken(signInResponser.getAccessToken());
                                myPreferences.setId(String.valueOf(signInResponser.getData().getUserId()));
                                myPreferences.setEmail(String.valueOf(signInResponser.getData().getEmail()));
                                myPreferences.setMobile(String.valueOf(signInResponser.getData().getMobileNumber()));
                                myPreferences.setUser_name(String.valueOf(signInResponser.getData().getName()));
                                myPreferences.setCity(String.valueOf(signInResponser.getData().getCityId()));
                                myPreferences.setCountry(String.valueOf(signInResponser.getData().getCountryId()));
                                myPreferences.setLogin("1");
                                myPreferences.setLogin_type(type);
                                Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                                intent.putExtra("event_id", "");
                                startActivity(intent);
                                finishAffinity();
                            } else if (response.getString("success").equalsIgnoreCase("201")) {
                                myStateview.showContent();
                                myPreferences.setEmail(response.getString("email"));
                                Intent intent = new Intent(SignInActivity.this, PasswordActivity.class);
                                startActivity(intent);
                            } else {
                                myStateview.showContent();
                                MyToast.display(SignInActivity.this, response.getString("message"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry = "2";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    public void getLocation() {
        GpsTracker gpsTracker = new GpsTracker(SignInActivity.this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            Log.e("Latitude", ">>>>>>>>" + latitude);
            Log.e("Longitude", ">>>>>>>>" + longitude);
            if (latitude > 0 && longitude > 0) {
                String address = getAddress(SignInActivity.this, latitude, longitude);
                // etAddress.setText(mAddress.get(0).getLocality()+", " +mAddress.get(0).getCountryName());
                Log.e("Address", ">>>>>>>>" + address);
                location = address;
            }
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    public static String getAddress(Context mContext, double lat, double lng) {
        List<Address> addresses = null;
        String add = null;
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            Log.e("", "MethodValue" + geocoder.getFromLocation(lat, lng, 1));

            if (addresses.size() > 0) {
                Address obj = addresses.get(0);
                add = obj.getAddressLine(0);
           /* add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();*/
                Log.v("IGA", "Address" + add);
            }
            Log.e("", "Method" + addresses.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return add;
    }

    @Override
    public void onRetryClick() {
      /*  if (retry.equalsIgnoreCase("1")) {

        }*/
    }

    @Override
    public void locationOn() {

    }

    @Override
    public void currentLocation(Location location) {
        Log.e("laat", "___________________>>" + location.getLatitude());
        Log.e("long", "___________________>>" + location.getLongitude());
        latitude = location.getLatitude();
        longitude = location.getLongitude();

    }

    @Override
    public void locationCancelled() {

    }
}
