package com.bam.issaguled.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.NotificationResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.UserNotificationActivity;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {



    private Context mCtx;
    //we are storing all the products in a list
    private int selectedPosition = 0;
    List<NotificationResponser.Data.Notification> notification;

    //getting the context and product list with constructor
    public NotificationAdapter(Context mCtx, List<NotificationResponser.Data.Notification> notification) {
        this.mCtx = mCtx;
        this.notification = notification;
    }

    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View listItem = inflater.inflate(R.layout.notificaion, parent, false);
        NotificationViewHolder viewHolder = new NotificationViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.NotificationViewHolder holder, int position) {
        //getting the product of the specified position

        holder.details.setText(notification.get(position).getMessage());

    }


    @Override
    public int getItemCount() {
        return notification.size();
    }


    class NotificationViewHolder extends RecyclerView.ViewHolder {
        TextView time, date, admin, details;


        public NotificationViewHolder(View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            date = itemView.findViewById(R.id.date);
            admin = itemView.findViewById(R.id.admin);
            details = itemView.findViewById(R.id.details);

        }

    }
}

