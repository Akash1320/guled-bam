package com.bam.issaguled.AdapterClasses;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.Admin.Activity.responser.UserAgendaResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.UserAgendaDetailsActivity;
import com.bam.issaguled.other.API_LIST;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserAgendaAdapter extends RecyclerView.Adapter<UserAgendaAdapter.ViewHolder> {
    private Context context;
    List<UserAgendaResponser.Data.Event> events;

    public UserAgendaAdapter(Context context, List<UserAgendaResponser.Data.Event> events) {
        this.context = context;
        this.events = events;
    }

    @NonNull
    @Override
    public UserAgendaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_row_agenda, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserAgendaAdapter.ViewHolder holder, final int position) {

        holder.tvAgendaName.setText(events.get(position).getTitle());
        Picasso.get().load(API_LIST.Base_url+events.get(position).getBackgroundImage())
                .placeholder(R.drawable.name)
                .into(holder.ivBackground);

        Picasso.get().load(API_LIST.Base_url+events.get(position).getIconImage())
                .placeholder(R.drawable.logo)
                .into(holder.ivIcon);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, UserAgendaDetailsActivity.class)
                .putExtra("agenda_id",String.valueOf(events.get(holder.getAdapterPosition()).getEventId())));
            }
        });


    }

    @Override
    public int getItemCount() {
        return events != null ? events.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAgendaName;
        private ImageView ivBackground;
        private ImageView ivoverlay;
        private ImageView ivIcon;
        private RelativeLayout llRoot;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAgendaName = itemView.findViewById(R.id.tvAgendaName);
            ivBackground = itemView.findViewById(R.id.ivBackground);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            llRoot = itemView.findViewById(R.id.llRoot);
        }
    }
}
