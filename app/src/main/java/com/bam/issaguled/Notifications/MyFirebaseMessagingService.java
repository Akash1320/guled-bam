package com.bam.issaguled.Notifications;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bam.issaguled.Admin.Activity.AdminHomeActivity;
import com.bam.issaguled.HomeActivity;
import com.bam.issaguled.SignInActivity;
import com.bam.issaguled.other.API_LIST;
import com.bam.issaguled.other.MyPreferences;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Dashspotless";
    private Bitmap bitmap;


    @Override
    public void onNewToken(@NonNull String s) {
        Log.e(TAG, "Refreshed token: " + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("onMessageReceived", "-------DATA--------" + remoteMessage.getData());
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(remoteMessage.getData().toString());
            Log.e("onMessageReceived", "-------DATA-JSON-------" + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (remoteMessage.getData() != null && remoteMessage.getData().toString().length() > 0) {
            String serverMessage = remoteMessage.getData().toString();
            serverMessage = serverMessage.substring(serverMessage.indexOf("=") + 1, serverMessage.length() - 1);

            MyNotificationManager mNotificationManager = new MyNotificationManager(getApplicationContext());
            Intent intent = null;
            if (new MyPreferences(getApplicationContext()).getLogin().equalsIgnoreCase("1")) {

                intent = new Intent(getApplicationContext(), HomeActivity.class);

            }else if (new MyPreferences(getApplicationContext()).getLogin().equalsIgnoreCase("2")) {

                intent = new Intent(getApplicationContext(), AdminHomeActivity.class);

            } else {
                intent = new Intent(getApplicationContext(), SignInActivity.class);

            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    /*        intent.putExtra("from", AppConstants.NOTIFICATION);
            intent.putExtra("merchant_id", mNotifier.getMerchant_id());
            intent.putExtra("merchant_name", mNotifier.getBusiness_name());
            intent.putExtra("merchant_logo", mNotifier.getBusiness_logo());
            intent.putExtra("type", mNotifier.getType());*/
            String imageUri = API_LIST.Base_url + remoteMessage.getData().get("icon_image");
            Log.e("title",">>>>>>>>>>>>>>>>>>>>>>"+ remoteMessage.getData().get("title"));
            bitmap = getBitmapfromUrl(imageUri);
            mNotificationManager.showSmallNotification("",
                    remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), bitmap, intent);
        } else {
            Log.e("serverMessage", "---------------GET DATA EMPTY");
        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
}