package com.bam.issaguled;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bam.issaguled.AdapterClasses.NotificationAdapter;
import com.bam.issaguled.AdapterClasses.UserAgendaAdapter;
import com.bam.issaguled.Admin.Activity.responser.UserAgendaResponser;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.all_event_of_user;
import static com.bam.issaguled.other.API_LIST.user_notification;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserNotificationActivity extends AppCompatActivity implements ProgressClickListener {
    Button donate;
    RecyclerView recyclerView;
    NotificationAdapter notificationAdapter;
    private Toolbar toolbar;
    private MyPreferences myPreferences;
    private MyStateview myStateview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notification);
        myPreferences= new MyPreferences(this);
        myStateview= new MyStateview(this,null);

        donate = findViewById(R.id.donate);
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recyclerview);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserNotificationActivity.this, DonateActivity.class);
                intent.putExtra("event_id","");
                startActivity(intent);
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(UserNotificationActivity.this));
        if(Methods.isOnline(this)){
            getNotifications();
        }

    }
    private void getNotifications() {
        myStateview.showLoading();
        AndroidNetworking.post(user_notification)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("user_id", myPreferences.getId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        NotificationResponser notificationResponser = gson.fromJson(response.toString(), NotificationResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                notificationAdapter = new NotificationAdapter(UserNotificationActivity.this, notificationResponser.getData().getNotification());
                                recyclerView.setAdapter(notificationAdapter);

                            } else if(response.getString("success").equalsIgnoreCase("404")){
                                MyToast.display(UserNotificationActivity.this, response.getString("message"));
                                myPreferences.clearPreferences();
                                startActivity(new Intent(UserNotificationActivity.this, SignInActivity.class));
                                finishAffinity();
                            }else {
                                myStateview.showContent();
                                MyToast.display(UserNotificationActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }


    @Override
    public void onRetryClick() {
        if(Methods.isOnline(this)){
            getNotifications();
        }
    }
}
