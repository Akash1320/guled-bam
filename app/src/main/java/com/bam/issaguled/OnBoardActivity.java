package com.bam.issaguled;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.overview;

public class OnBoardActivity extends AppCompatActivity implements ProgressClickListener {
    Button Donate, signin, signup, skip;
    private TextView[] dots;
    private int[] layouts;
    private LinearLayout dotsLayout;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private TextView tvRegisteredUser;
    private String[] permissions = new String[]
            {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET,
                    Manifest.permission.INSTALL_LOCATION_PROVIDER,
            };
    private int REQUEST_ID_MULTIPLE_PERMISSIONS = 0;
    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(OnBoardActivity.this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(OnBoardActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board);
        //checkPermissions();
        Donate = (Button) findViewById(R.id.donate);
        signin = (Button) findViewById(R.id.signin);
        signup = (Button) findViewById(R.id.signup);
        tvRegisteredUser = findViewById(R.id.tvRegisteredUser);
        skip = (Button) findViewById(R.id.skip);
        myStateview = new MyStateview(this, null);
        myPreferences = new MyPreferences(this);

        Donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreferences.getLogin().equalsIgnoreCase("")) {
                    startActivity(new Intent(OnBoardActivity.this, SignInActivity.class));
                } else {
                    Intent intent = new Intent(OnBoardActivity.this, DonateActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnBoardActivity.this, SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnBoardActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnBoardActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        if (Methods.isOnline(this)) {
            getOverView();
        }
    }

    private void getOverView() {
        myStateview.showLoading();
        AndroidNetworking.get(overview)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                tvRegisteredUser.setText(response.getJSONObject("data").getString("register_users"));

                            } else {
                                myStateview.showContent();
                                MyToast.display(OnBoardActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {
        getOverView();

    }
}
