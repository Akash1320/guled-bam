package com.bam.issaguled;

import android.content.Intent;
import android.os.Bundle;

import com.bam.issaguled.AdapterClasses.UserAgendaAdapter;
import com.bam.issaguled.Admin.Activity.AdminHomeActivity;
import com.bam.issaguled.Admin.Activity.responser.UserAgendaResponser;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import static com.bam.issaguled.other.API_LIST.all_event_of_user;
import static com.bam.issaguled.other.API_LIST.all_event_without_login;

public class HomeActivity extends AppCompatActivity implements ProgressClickListener {
    public static TextView title;
    public static ImageView back;
    public static FloatingActionButton add;
    private RecyclerView recyclerview;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    ImageView ivProfile;
    ImageView ivNotification;
    private Button donate;
    private SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        donate = findViewById(R.id.donate);
        swipeRefresh = findViewById(R.id.swipeRefresh);
        ivProfile = findViewById(R.id.ivProfile);
        ivNotification = findViewById(R.id.ivNotification);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(false);
                if (Methods.isOnline(HomeActivity.this)) {
                    if (myPreferences.getLogin().equalsIgnoreCase("")) {
                        getAgendaswithoulogin();
                    } else {
                        getAgendas();
                    }
                }
            }
        });
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, UserProfileActivity.class));
            }
        });

        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myPreferences.getLogin().equalsIgnoreCase("")){
                    startActivity(new Intent(HomeActivity.this, SignInActivity.class));
                }else {
                    startActivity(new Intent(HomeActivity.this, DonateActivity.class)
                    .putExtra("event_id",""));
                }
            }
        });

        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, UserNotificationActivity.class));
            }
        });

        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);


        if (myPreferences.getLogin().equalsIgnoreCase("")) {
            ivProfile.setVisibility(View.GONE);
            ivNotification.setVisibility(View.GONE);
        }

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new GridLayoutManager(this, 2));

        if (Methods.isOnline(this)) {
            if (myPreferences.getLogin().equalsIgnoreCase("")) {
                getAgendaswithoulogin();
            } else {
                getAgendas();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onResume() {
        super.onResume();
    }

    private void getAgendas() {
        myStateview.showLoading();
        AndroidNetworking.post(all_event_of_user)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("user_id", myPreferences.getId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        UserAgendaResponser userAgendaResponser = gson.fromJson(response.toString(), UserAgendaResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                UserAgendaAdapter adapter = new UserAgendaAdapter(HomeActivity.this, userAgendaResponser.getData().getEvent());
                                recyclerview.setAdapter(adapter);

                            } else if(response.getString("success").equalsIgnoreCase("404")){
                                MyToast.display(HomeActivity.this, response.getString("message"));
                                myPreferences.clearPreferences();
                                startActivity(new Intent(HomeActivity.this, SignInActivity.class));
                                finishAffinity();
                            }else {
                                myStateview.showContent();
                                MyToast.display(HomeActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }
    private void getAgendaswithoulogin() {
        myStateview.showLoading();
        AndroidNetworking.get(all_event_without_login)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        UserAgendaResponser userAgendaResponser = gson.fromJson(response.toString(), UserAgendaResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                UserAgendaAdapter adapter = new UserAgendaAdapter(HomeActivity.this, userAgendaResponser.getData().getEvent());
                                recyclerview.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(HomeActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    @Override
    public void onRetryClick() {
        if (myPreferences.getLogin().equalsIgnoreCase("")) {
            getAgendaswithoulogin();
        } else {
            getAgendas();
        }
    }
}
