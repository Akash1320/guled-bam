package com.bam.issaguled;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;
import static com.bam.issaguled.other.API_LIST.event_details_without_login;
import static com.bam.issaguled.other.API_LIST.get_user_event_details;
import static com.bam.issaguled.other.API_LIST.user_event_status;

public class UserAgendaDetailsActivity extends AppCompatActivity implements ProgressClickListener {
    ImageView yes, no;
    private TextView userYes, userNO;
    ProgressBar progressBar;
    Button donate, submit;
    private Toolbar toolbar;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private String agenda_id;
    private TextView textView40;
    private TextView tvHeader;
    private String status = "";
    private ScrollView scrollView;
    private LinearLayout llPercentage;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_second);

        if (getIntent() != null) {
            agenda_id = getIntent().getStringExtra("agenda_id");
        }

        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        tvHeader = findViewById(R.id.tvHeader);
        scrollView = findViewById(R.id.scrollView);
        textView40 = findViewById(R.id.textView40);
        progressBar = findViewById(R.id.progressBar1);
        donate = findViewById(R.id.donate);
        yes = findViewById(R.id.img_yes);
        no = findViewById(R.id.img_no);
        userYes = findViewById(R.id.yes);
        userNO = findViewById(R.id.no);
        submit = findViewById(R.id.submit);
        toolbar = findViewById(R.id.toolbar);
        llPercentage = findViewById(R.id.llPercentage);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            textView40.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("login", ">>>>>>>>>>>>>>>>>>>>" + myPreferences.getLogin());
                if (myPreferences.getLogin().equalsIgnoreCase("")) {
                    Intent intent = new Intent(UserAgendaDetailsActivity.this, SignInActivity.class);
                    startActivity(intent);
                } else if (status.equalsIgnoreCase("")) {
                    MyToast.display(UserAgendaDetailsActivity.this, "Please Select Status");
                } else {
                    if (Methods.isOnline(UserAgendaDetailsActivity.this)) {
                        postInterest();
                    }
                }
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "yes";
                yes.setBackgroundResource(R.drawable.yes_active);
                no.setBackgroundResource(R.drawable.no_inactive);
                submit.setVisibility(View.VISIBLE);
                scrollView.smoothScrollTo(0, scrollView.getBottom());
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "no";
                no.setBackgroundResource(R.drawable.no_active);
                yes.setBackgroundResource(R.drawable.yes_inactive);
                submit.setVisibility(View.VISIBLE);
                scrollView.smoothScrollTo(0, scrollView.getBottom());
            }
        });

        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreferences.getLogin().equalsIgnoreCase("")) {
                    startActivity(new Intent(UserAgendaDetailsActivity.this, SignInActivity.class));
                } else {
                    startActivity(new Intent(UserAgendaDetailsActivity.this, DonateActivity.class)
                    .putExtra("event_id",agenda_id));
                }
            }
        });

        if (Methods.isOnline(this)) {
            if (myPreferences.getLogin().equalsIgnoreCase("")) {
                getDetailswithoutLogin();
            } else {
                getDetails();
            }
        }
    }

    private void getDetails() {
        myStateview.showLoading();
        AndroidNetworking.post(get_user_event_details)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("event_id", agenda_id)
                .addBodyParameter("login_id", myPreferences.getId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                tvHeader.setText(response.getJSONObject("data").getString("title"));
                                textView40.setText(response.getJSONObject("data").getString("description"));
                                if (response.getJSONObject("data").getString("status").equalsIgnoreCase("Accepted")) {
                                    yes.setBackgroundResource(R.drawable.yes_active);
                                    no.setBackgroundResource(R.drawable.no_inactive);
                                    yes.setEnabled(false);
                                    no.setEnabled(false);
                                    submit.setVisibility(View.INVISIBLE);
                                } else if(response.getJSONObject("data").getString("status").equalsIgnoreCase("Rejected")) {
                                    no.setBackgroundResource(R.drawable.no_active);
                                    yes.setBackgroundResource(R.drawable.yes_inactive);
                                    submit.setVisibility(View.INVISIBLE);
                                    yes.setEnabled(false);
                                    no.setEnabled(false);
                                }
                                float accepted = Integer.parseInt(response.getJSONObject("data").getString("total_attendees_users"));
                                float rejected = Integer.parseInt(response.getJSONObject("data").getString("not_attendees_users"));

                                float total_count= accepted+rejected;
                                Log.e("accepted",">>>>>>>>>>>>>>>>>>>>>>>"+accepted);
                                Log.e("rejected",">>>>>>>>>>>>>>>>>>>>>>>"+rejected);
                                Log.e("total_count",">>>>>>>>>>>>>>>>>>>>>>>"+total_count);

                                if(total_count!=0) {
                                    llPercentage.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.VISIBLE);
                                    if (accepted != 0.0) {
                                        float yes = accepted / total_count;
                                        Log.e("yes", ">>>>>>>>>>>>>>>>>>>>>>>" + yes);
                                        userYes.setText(new DecimalFormat("##.#").format(yes * 100) + "%");
                                        userNO.setText(new DecimalFormat("##.#").format(100 - yes * 100) + "%");
                                        int p = (int) (yes * 100);
                                        progressBar.setProgress(p);
                                    } else {
                                        float yes = rejected / total_count;
                                        Log.e("yes", ">>>>>>>>>>>>>>>>>>>>>>>" + yes);
                                        userNO.setText(new DecimalFormat("##.#").format(yes * 100) + "%");
                                        userYes.setText(new DecimalFormat("##.#").format(100 - yes * 100) + "%");
                                        if(yes==1.0){
                                            progressBar.setProgress(0);
                                        }else {
                                            int p = (int) (yes * 100);
                                            progressBar.setProgress(p);
                                        }
                                    }
                                }else {
                                    llPercentage.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                }

                                /*float invited = Integer.parseInt(response.getJSONObject("data").getString("total_invited_users"));
                                float attendee = Integer.parseInt(response.getJSONObject("data").getString("total_attendees_users"));
                                float progress = attendee / invited;
                                int p = (int) (progress * 100);
                                progressBar.setProgress(p);
                                Log.e("p", ">>>>>>>>>>>>>>>>>>" + p);
                                userYes.setText(new DecimalFormat("##.#").format(progress * 100) + "%");
                                userNO.setText(new DecimalFormat("##.#").format(100 - progress * 100) + "%");*/
                            } else {
                                myStateview.showContent();
                                MyToast.display(UserAgendaDetailsActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void getDetailswithoutLogin() {
        myStateview.showLoading();
        AndroidNetworking.post(event_details_without_login)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addBodyParameter("event_id", agenda_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                tvHeader.setText(response.getJSONObject("data").getString("title"));
                                textView40.setText(response.getJSONObject("data").getString("description"));
                                float invited = Integer.parseInt(response.getJSONObject("data").getString("total_invited_users"));
                                float attendee = Integer.parseInt(response.getJSONObject("data").getString("total_attendees_users"));
                                float progress = attendee / invited;
                                int p = (int) (progress * 100);
                                progressBar.setProgress(p);
                                Log.e("p", ">>>>>>>>>>>>>>>>>>" + p);
                                userYes.setText(new DecimalFormat("##.#").format(progress * 100) + "%");
                                userNO.setText(new DecimalFormat("##.#").format(100 - progress * 100) + "%");
                            } else {
                                myStateview.showContent();
                                MyToast.display(UserAgendaDetailsActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }
    private void postInterest() {
        myStateview.showLoading();
        AndroidNetworking.post(user_event_status)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("event_id", agenda_id)
                .addBodyParameter("user_id", myPreferences.getId())
                .addBodyParameter("do_you_want_to_attend", status)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                MyToast.display(UserAgendaDetailsActivity.this, "Thanks for your valuable Vote");
                                getDetails();
                            } else {
                                myStateview.showContent();
                                MyToast.display(UserAgendaDetailsActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }
    @Override
    public void onRetryClick() {
        if(Methods.isOnline(this)){
            postInterest();
        }
    }
}
