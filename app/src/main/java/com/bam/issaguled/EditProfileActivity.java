package com.bam.issaguled;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bam.issaguled.Admin.Activity.responser.SelectAgendaResponser;
import com.bam.issaguled.Admin.Activity.responser.SignInResponser;
import com.bam.issaguled.other.API_LIST;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.bam.issaguled.other.API_LIST.all_cities;
import static com.bam.issaguled.other.API_LIST.update_admin_detail;
import static com.bam.issaguled.other.API_LIST.update_user_detail;
import static com.bam.issaguled.other.API_LIST.user_register;

public class EditProfileActivity extends AppCompatActivity implements ProgressClickListener {

    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private Toolbar toolbar;
    private EditText name;
    private EditText edtEmail;
    private EditText edtMobile;
    private Spinner city;
    private String city_id = "";
    private ArrayList<String> cityList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> cityResponserArrayList;
    private Button btnProfile;
    private String mName;
    private String mEmail;
    private String mMobile;
    private String mCity;
    private String mCountry;
    private String mState;
    private String mLocation;
    private Spinner country;
    private Spinner state;
    private String country_id = "";
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> countryResponserArrayList;
    private String retry_status = "";
    private String state_id = "";
    private ArrayList<String> stateList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> stateResponserArrayList;
    private String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        if (getIntent() != null) {
            tag = getIntent().getStringExtra("tag");
            if (tag.equalsIgnoreCase("admin")) {
                mName = getIntent().getStringExtra("name");
                mEmail = getIntent().getStringExtra("email");
                mMobile = getIntent().getStringExtra("mobile");
                mCity = getIntent().getStringExtra("city");
                mCountry = getIntent().getStringExtra("country");
                mState = getIntent().getStringExtra("state");
            } else {
                mName = getIntent().getStringExtra("name");
                mEmail = getIntent().getStringExtra("email");
                mMobile = getIntent().getStringExtra("mobile");
                mCity = getIntent().getStringExtra("city");
                mCountry = getIntent().getStringExtra("country");
                mLocation = getIntent().getStringExtra("location");
                mState = getIntent().getStringExtra("state");
            }
        }


        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        toolbar = findViewById(R.id.toolbar);
        country = (Spinner) findViewById(R.id.country);
        state = (Spinner) findViewById(R.id.state);
        btnProfile = findViewById(R.id.btnProfile);
        city = findViewById(R.id.city);
        edtMobile = findViewById(R.id.edtMobile);
        edtEmail = findViewById(R.id.edtEmail);
        name = findViewById(R.id.name);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);

        name.setText(mName);
        edtEmail.setText(mEmail);
        edtMobile.setText(mMobile);
        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    country_id = countryResponserArrayList.get(position - 1).getId();
                    Log.e("country_id", "-----------" + country_id);
                    getStateList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    state_id = stateResponserArrayList.get(position - 1).getId();
                    Log.e("state_id", "-----------" + state_id);
                    getCityList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                city_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    city_id = cityResponserArrayList.get(position - 1).getId();
                    Log.e("city_id", "-----------" + city_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (Methods.isOnline(this)) {
            getCountryList();
        }

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().isEmpty()) {
                    MyToast.display(EditProfileActivity.this, "Please Enter Name");
                } else if (edtEmail.getText().toString().isEmpty()) {
                    MyToast.display(EditProfileActivity.this, "Please Enter Email");
                } else if (edtMobile.getText().toString().isEmpty()) {
                    MyToast.display(EditProfileActivity.this, "Please Enter Mobile Number");
                } else if (edtMobile.getText().toString().length() != 14) {
                    MyToast.display(EditProfileActivity.this, "Please Enter Valid Mobile Number");
                } else if (country_id.isEmpty()) {
                    MyToast.display(EditProfileActivity.this, "Please Select Country");
                } else if (state_id.isEmpty()) {
                    MyToast.display(EditProfileActivity.this, "Please Select State");
                } else if (city_id.isEmpty()) {
                    MyToast.display(EditProfileActivity.this, "Please Select City");
                } else {
                    if (Methods.isOnline(EditProfileActivity.this)) {
                        if (tag.equalsIgnoreCase("admin")) {
                            adminUpdateProfile();
                        } else {
                            updateProfile();
                        }
                    }
                }
            }
        });

    }

    private void getCountryList() {
        myStateview.showLoading();
        AndroidNetworking.get(API_LIST.all_countries)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                countryList.add("Select Country");
                                countryResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("country_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    countryList.add(jsonObject.getString("name"));
                                    countryResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.spinner_list, countryList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                country.setAdapter(adapter);
                                country.setSelection(countryList.indexOf(mCountry));

                            } else {
                                myStateview.showContent();
                                MyToast.display(EditProfileActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status = "2";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void getCityList() {
        if (!cityList.isEmpty()) {
            cityList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.post(all_cities)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addBodyParameter("state_id", state_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                cityList.add("Select City");
                                cityResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("city_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    cityList.add(jsonObject.getString("name"));
                                    cityResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.spinner_list, cityList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                city.setAdapter(adapter);

                                city.setSelection(cityList.indexOf(mCity));

                            } else {
                                myStateview.showContent();
                                MyToast.display(EditProfileActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status = "5";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void updateProfile() {
        myStateview.showLoading();
        Log.e("token", ">>>>>>>>>>>>>>>>>>>>>>>>>" + myPreferences.getToken());
        Log.e("user_id", ">>>>>>>>>>>>>>>>>>>>>>>>>" + myPreferences.getId());
        Log.e("name", ">>>>>>>>>>>>>>>>>>>>>>>>>" + name.getText().toString());
        Log.e("email", ">>>>>>>>>>>>>>>>>>>>>>>>>" + edtEmail.getText().toString());
        Log.e("city_id", ">>>>>>>>>>>>>>>>>>>>>>>>>" + city_id);
        Log.e("mobile_number", ">>>>>>>>>>>>>>>>>>>>>>>>>" + edtMobile.getText().toString());
        Log.e("location", ">>>>>>>>>>>>>>>>>>>>>>>>>" + mLocation);
        AndroidNetworking.post(update_user_detail)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("user_id", myPreferences.getId())
                .addBodyParameter("name", name.getText().toString())
                .addBodyParameter("email", edtEmail.getText().toString())
                .addBodyParameter("city_id", city_id)
                .addBodyParameter("country_id", country_id)
                .addBodyParameter("state_id", state_id)
                .addBodyParameter("mobile_number", edtMobile.getText().toString())
                .addBodyParameter("location", mLocation)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                finish();
                            } else {
                                myStateview.showContent();
                                MyToast.display(EditProfileActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status = "1";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }

    private void adminUpdateProfile() {
        myStateview.showLoading();
        Log.e("token", ">>>>>>>>>>>>>>>>>>>>>>>>>" + myPreferences.getToken());
        Log.e("user_id", ">>>>>>>>>>>>>>>>>>>>>>>>>" + myPreferences.getId());
        Log.e("name", ">>>>>>>>>>>>>>>>>>>>>>>>>" + name.getText().toString());
        Log.e("email", ">>>>>>>>>>>>>>>>>>>>>>>>>" + edtEmail.getText().toString());
        Log.e("city_id", ">>>>>>>>>>>>>>>>>>>>>>>>>" + city_id);
        Log.e("state", ">>>>>>>>>>>>>>>>>>>>>>>>>" + state_id);
        Log.e("mobile_number", ">>>>>>>>>>>>>>>>>>>>>>>>>" + edtMobile.getText().toString());
        AndroidNetworking.post(update_admin_detail)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("admin_id", myPreferences.getId())
                .addBodyParameter("name", name.getText().toString())
                .addBodyParameter("email", edtEmail.getText().toString())
                .addBodyParameter("city", city_id)
                .addBodyParameter("country", country_id)
                .addBodyParameter("state", state_id)
                .addBodyParameter("mobile_number", edtMobile.getText().toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                finish();
                            } else {
                                myStateview.showContent();
                                MyToast.display(EditProfileActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status = "4";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }


    private void getStateList() {
        if (!stateList.isEmpty()) {
            stateList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.post(API_LIST.all_state)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addBodyParameter("country_id", country_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                stateList.add("Select State");
                                stateResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("state_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    stateList.add(jsonObject.getString("name"));
                                    stateResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.spinner_list, stateList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                state.setAdapter(adapter);
                                state.setSelection(stateList.indexOf(mState));
                            } else {
                                myStateview.showContent();
                                MyToast.display(EditProfileActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status = "3";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    @Override
    public void onRetryClick() {
        myStateview.showContent();
        if (retry_status.equalsIgnoreCase("1")) {
            updateProfile();
        } else if (retry_status.equalsIgnoreCase("2")) {
            getCountryList();
        } else if (retry_status.equalsIgnoreCase("3")) {
            getStateList();
        } else if (retry_status.equalsIgnoreCase("4")) {
            adminUpdateProfile();
        } else {
            getCityList();
        }

    }
}
