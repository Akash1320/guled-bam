package com.bam.issaguled;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.bam.issaguled.Admin.Activity.AdminHomeActivity;
import com.bam.issaguled.other.MyPreferences;

public class SplashActivity extends AppCompatActivity {
    private static final int SPLASH_DURATION = 2000;
    Handler handler;
    private MyPreferences myPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myPreferences = new MyPreferences(this);
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(myPreferences.getLogin().equalsIgnoreCase("1")){
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }else if(myPreferences.getLogin().equalsIgnoreCase("2")){
                    Intent intent = new Intent(SplashActivity.this, AdminHomeActivity.class);
                    startActivity(intent);
                    finishAffinity();
                }else if (myPreferences.getFirst_time().equalsIgnoreCase("")) {
                    myPreferences.setFirst_time("1");
                    Intent intent = new Intent(SplashActivity.this, OnBoardActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, SignInActivity.class);
                    startActivity(intent);
                    finish();
                }
                // Toast.makeText(SplashActivity.this," ewkjdsfh sd", Toast.LENGTH_SHORT).show();

            }
        }, SPLASH_DURATION);

    }
}
