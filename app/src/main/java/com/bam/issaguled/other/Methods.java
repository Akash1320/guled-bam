package com.bam.issaguled.other;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;

import com.bam.issaguled.R;
import com.bam.issaguled.SignInActivity;


public class Methods implements ProgressClickListener {
    public static  String CURRENCY;

    public static boolean isOnline(final Activity activity) {
        ConnectivityManager conMgr = (ConnectivityManager) activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            MyToast.display(activity, "No Internet Connection");
            return false;
        }
        return true;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static void forceLogoutDialog(final Activity activity) {
        AlertDialog logoutDialog = new AlertDialog.Builder(activity, R.style.CustomDialogTheme)
                .setTitle("Another user logged in with this ID")
                .setCancelable(false)
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new MyPreferences(activity).clearPreferences();
                        activity.startActivity(new Intent(activity, SignInActivity.class));
                        activity.finishAffinity();


                    }
                }).create();//.create().show();
        logoutDialog.show();
        Button nbutton = logoutDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        Button pbutton = logoutDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
    }

    public static void logPrint(String name, String value) {
        Log.e(name, ">>>>>>>>>>>>>>>>>>>>>>>>>" + value);
    }

    @Override
    public void onRetryClick() {

    }
}
