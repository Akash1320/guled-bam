package com.bam.issaguled.other;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MyPreferences {
    private static MyPreferences preferences = null;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private String id = "id";
    private String token = "token";
    private String login = "login";
    private String totalprice = "totalprice";
    private String subscription_id = "subscription_id";
    private String subscription_discount = "subscription_discount";
    private String user_name = "user_name";
    private String email = "email";
    private String mobile = "mobile";
    private String lat = "lat";
    private String Long = "long";
    private String wallet = "wallet";
    private String paidAmount = "paidAmount";
    private String first_time="first_time";
    private String country="country";
    private String city="city";
    private String login_type="login_type";

    public MyPreferences(Context context) {
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    public void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }


    public static MyPreferences getActiveInstance(Context context) {
        if (preferences == null) {
            preferences = new MyPreferences(context);
        }
        return preferences;
    }

    public void setLogin(String login) {
        editor = mPreferences.edit();
        editor.putString(this.login, login);

        editor.commit();
    }

    public String getLogin() {
        return mPreferences.getString(this.login, "");
    }

    public void setLogin_type(String login_type) {
        editor = mPreferences.edit();
        editor.putString(this.login_type, login_type);

        editor.commit();
    }

    public String getLogin_type() {
        return mPreferences.getString(this.login_type, "");
    }

    public void setCity(String city) {
        editor = mPreferences.edit();
        editor.putString(this.city, city);

        editor.commit();
    }

    public String getCity() {
        return mPreferences.getString(this.city, "");
    }

    public void setCountry(String country) {
        editor = mPreferences.edit();
        editor.putString(this.country, country);

        editor.commit();
    }

    public String getCountry() {
        return mPreferences.getString(this.country, "");
    }

    public void setFirst_time(String first_time) {
        editor = mPreferences.edit();
        editor.putString(this.first_time, first_time);

        editor.commit();
    }

    public String getFirst_time() {
        return mPreferences.getString(this.first_time, "");
    }

    public String getToken() {
        return mPreferences.getString(this.token, "");
    }

    public void setToken(String token) {
        editor = mPreferences.edit();
        editor.putString(this.token, token);
        editor.commit();
    }

    public String getLat() {
        return mPreferences.getString(this.lat, "");
    }

    public void setLat(String lat) {
        editor = mPreferences.edit();
        editor.putString(this.lat, lat);
        editor.commit();
    }

    public String getLong() {
        return mPreferences.getString(this.Long, "");
    }

    public void setLong(String Long) {
        editor = mPreferences.edit();
        editor.putString(this.Long, Long);
        editor.commit();
    }
    public String getWallet() {
        return mPreferences.getString(this.wallet, "");
    }

    public void setWallet(String wallet) {
        editor = mPreferences.edit();
        editor.putString(this.wallet, wallet);
        editor.commit();
    }

    public String getUser_name() {
        return mPreferences.getString(this.user_name, "");

    }

    public void setUser_name(String user_name) {
        editor = mPreferences.edit();
        editor.putString(this.user_name, user_name);
        editor.commit();
    }

    public String getPaidAmount() {
        return mPreferences.getString(this.paidAmount, "");

    }

    public void setPaidAmount(String paidAmount) {
        editor = mPreferences.edit();
        editor.putString(this.paidAmount, paidAmount);
        editor.commit();
    }

    public String getEmail() {
        return mPreferences.getString(this.email, "");

    }

    public void setEmail(String email) {
        editor = mPreferences.edit();
        editor.putString(this.email, email);
        editor.commit();
    }


    public String getMobile() {
        return mPreferences.getString(this.mobile, "");

    }

    public void setMobile(String mobile) {
        editor = mPreferences.edit();
        editor.putString(this.mobile, mobile);
        editor.commit();
    }

    public String getTotalprice() {
        return mPreferences.getString(this.totalprice, "");

    }

    public void setTotalprice(String totalprice) {
        editor = mPreferences.edit();
        editor.putString(this.totalprice, totalprice);
        editor.commit();
    }

    public String getSubscription_id() {
        return mPreferences.getString(this.subscription_id, "");

    }

    public void setSubscription_id(String subscription_id) {
        editor = mPreferences.edit();
        editor.putString(this.subscription_id, subscription_id);
        editor.commit();
    }

    public String getSubscription_discount() {
        return mPreferences.getString(this.subscription_discount, "");

    }

    public void setSubscription_discount(String subscription_discount) {
        editor = mPreferences.edit();
        editor.putString(this.subscription_discount, subscription_discount);
        editor.commit();
    }

    public String getId() {
        return mPreferences.getString(this.id, "");
    }

    public void setId(String id) {
        editor = mPreferences.edit();
        editor.putString(this.id, id);
        editor.commit();
    }

    public void clearPreferences() {
        editor = mPreferences.edit();
        editor.clear();
        editor.commit();
    }

}
