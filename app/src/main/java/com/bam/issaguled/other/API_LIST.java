package com.bam.issaguled.other;

public class API_LIST {
   //public static final String Base_url="http://alobhatech.com/bam_app/public/";
   public static final String Base_url="http://34.248.123.205/bam_app/public/";
    public static final String admin_signIN =Base_url+"api/auth/admin/login";
    public static final String user_signIN =Base_url+"api/auth/user/login";
    public static final String get_all_events =Base_url+"api/auth/admin/get-all-events";
    public static final String agenda_subject_store =Base_url+"api/auth/admin/agenda-subject-store";
    public static final String unassign_agenda_subject =Base_url+"api/auth/admin/unassign-agenda-subject";
    public static final String agenda_store =Base_url+"api/auth/admin/agenda-store";
    public static final String event_details =Base_url+"api/auth/admin/event-details";
    public static final String event_invited_users =Base_url+"api/auth/admin/event-invited-users";
    public static final String get_all_users =Base_url+"api/auth/admin/get-all-users";
    public static final String all_countries =Base_url+"api/auth/user/all-countries";
    public static final String all_state =Base_url+"api/auth/user/all-states";
    public static final String all_cities =Base_url+"api/auth/user/all-cities";
    public static final String setting_page_contain =Base_url+"api/auth/admin/setting-page-contain";
    public static final String user_create =Base_url+"api/auth/admin/user-create";
    public static final String get_all_donations =Base_url+"api/auth/admin/get-all-donations";
    public static final String user_donation_details =Base_url+"api/auth/admin/user_donation_details";
    public static final String edit_profile =Base_url+"api/auth/admin/edit-profile";
    public static final String user_register =Base_url+"api/auth/user/user-register";
    public static final String user_register_social =Base_url+"api/auth/user/user-register-social";
    public static final String all_event_of_user =Base_url+"api/auth/user/all-event-of-user";
    public static final String get_user_detail =Base_url+"api/auth/user/get-user-detail";
    public static final String get_user_event_details =Base_url+"api/auth/user/get-user-event-details";
    public static final String overview =Base_url+"api/auth/user/overview";
    public static final String user_event_status =Base_url+"api/auth/user/user-event-status";
    public static final String all_event_without_login =Base_url+"api/auth/user/all_event_without_login";
    public static final String event_details_without_login =Base_url+"api/auth/user/event_details_without_login";
    public static final String add_donation_with_login =Base_url+"api/auth/user/add-donation-with-login";
    public static final String user_notification =Base_url+"api/auth/user/user_notification";
    public static final String update_user_detail =Base_url+"api/auth/user/update-user-detail";
    public static final String update_admin_detail =Base_url+"api/auth/admin/profile";
    public static final String admin_notification =Base_url+"api/auth/admin/admin_notification";
    public static final String agenda_update =Base_url+"api/auth/admin/agenda-update";
    public static final String agendaDelete =Base_url+"api/auth/admin/agendaDelete";
}