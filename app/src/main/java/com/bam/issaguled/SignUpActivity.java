package com.bam.issaguled;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;


import com.bam.issaguled.Admin.Activity.responser.SelectAgendaResponser;
import com.bam.issaguled.Admin.Activity.responser.SignInResponser;
import com.bam.issaguled.other.API_LIST;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.all_cities;
import static com.bam.issaguled.other.API_LIST.user_register;

public class SignUpActivity extends AppCompatActivity implements ProgressClickListener {

    TextView text_signin;
    Button Register;
    TextInputEditText name, email, number,edtCountry,edtCity;
    TextInputLayout NumberinputLyout, EmailinputLyout, CityinputLyout, NameinputLyout;
    private MyStateview myStateview;
    private ArrayList<String> cityList = new ArrayList<>();
    private ArrayList<String> stateList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> cityResponserArrayList;
    private ArrayList<SelectAgendaResponser> stateResponserArrayList;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> countryResponserArrayList;
    private Spinner city;
    private Spinner country;
    private Spinner state;
    private String city_id = "";
    private String[] permissions = new String[]
            {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET,
                    Manifest.permission.INSTALL_LOCATION_PROVIDER,
            };
    private int REQUEST_ID_MULTIPLE_PERMISSIONS = 0;
    private String country_id="";
    private String state_id="";
    private String retry_status="";

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(SignUpActivity.this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(SignUpActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
      //  checkPermissions();
        myStateview = new MyStateview(this
                , null);

        text_signin = (TextView) findViewById(R.id.text_signin);
        Register = (Button) findViewById(R.id.signup);
        name = (TextInputEditText) findViewById(R.id.name);
        email = (TextInputEditText) findViewById(R.id.email);
        city = (Spinner) findViewById(R.id.city);
        country = (Spinner) findViewById(R.id.country);
        state = (Spinner) findViewById(R.id.state);

        number = (TextInputEditText) findViewById(R.id.number);
        NumberinputLyout = (TextInputLayout) findViewById(R.id.numberinputLyout);
        EmailinputLyout = (TextInputLayout) findViewById(R.id.emailinputLyout);
        CityinputLyout = (TextInputLayout) findViewById(R.id.cityinputLyout);
        NameinputLyout = (TextInputLayout) findViewById(R.id.nameinputLyout);

        text_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });

        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    country_id = countryResponserArrayList.get(position - 1).getId();
                    Log.e("country_id", "-----------" + country_id);
                    getStateList();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

     city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                city_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    city_id = cityResponserArrayList.get(position - 1).getId();
                    Log.e("city_id", "-----------" + city_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

     state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    state_id = stateResponserArrayList.get(position - 1).getId();
                    Log.e("state_id", "-----------" + state_id);
                    getCityList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (Methods.isOnline(this)) {
            getCountryList();
        }
    }
    private void validate() {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String Email = email.getText().toString().trim();
        String Name = name.getText().toString().trim();
        String Number = number.getText().toString().trim();
        if (TextUtils.isEmpty(Name)) {
            MyToast.display(SignUpActivity.this, "Please Enter Name");
            // Toast.makeText(this,"Please Enter Name",Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(Email) || Email.equals("")) {
            MyToast.display(SignUpActivity.this, "Please Enter Email");
            // Toast.makeText(this,"Please Enter Email",Toast.LENGTH_SHORT).show();
            // email.setError("Please Enter Email");
            return;
        } else if (!Email.matches(emailPattern)) {
            MyToast.display(SignUpActivity.this, "Please Enter Valid Email Address");
            //  RegEmail.setError("Please enter right email Address");
            // Toast.makeText(this,"Please enter right email Address",Toast.LENGTH_SHORT).show();
            return;

        } else if (TextUtils.isEmpty(Number)) {
            MyToast.display(SignUpActivity.this, "Please Enter Mobile Number");
            // number.setError("Please Enter Number");
            // Toast.makeText(this,"Please Enter Number",Toast.LENGTH_SHORT).show();
            return;
        } else if (!(Number.length() == 14)) {
            MyToast.display(SignUpActivity.this, "Please Enter Valid Mobile Number");
            //  RegEmail.setError("Please enter right email Address");
            //  Toast.makeText(this,"Please Enter a valid mobile number",Toast.LENGTH_SHORT).show();
            return;
        }  else if (TextUtils.isEmpty(country_id)) {
            MyToast.display(SignUpActivity.this, "Please Select Country");
            //Toast.makeText(this,"Please Enter City",Toast.LENGTH_SHORT).show();
            return;
        }  else if (TextUtils.isEmpty(state_id)) {
            MyToast.display(SignUpActivity.this, "Please Select State");
            //Toast.makeText(this,"Please Enter City",Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(city_id)) {
            MyToast.display(SignUpActivity.this, "Please Select City");
            //Toast.makeText(this,"Please Enter City",Toast.LENGTH_SHORT).show();
            return;
        } else {
            CityinputLyout.setErrorEnabled(false);
            signup();
        }
    }

    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void signup() {
        myStateview.showLoading();
        AndroidNetworking.post(user_register)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addBodyParameter("name", name.getText().toString())
                .addBodyParameter("email", email.getText().toString())
                .addBodyParameter("city_id", city_id)
                .addBodyParameter("country_id", country_id)
                .addBodyParameter("state_id", state_id)
                .addBodyParameter("mobile_number", number.getText().toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        SignInResponser signInResponser = gson.fromJson(response.toString(), SignInResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                MyToast.display(SignUpActivity.this, response.getString("message"));
                                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            } else {
                                myStateview.showContent();
                                MyToast.display(SignUpActivity.this, response.getString("message"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status="1";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void getCityList() {
        if(!cityList.isEmpty()){
            cityList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.post(all_cities)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addBodyParameter("state_id", state_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                cityList.add("Select City");
                                cityResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("city_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    cityList.add(jsonObject.getString("name"));
                                    cityResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.spinner_list, cityList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                city.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(SignUpActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status="4";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }
    private void getCountryList() {

        myStateview.showLoading();
        AndroidNetworking.get(API_LIST.all_countries)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                countryList.add("Select Country");
                                countryResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("country_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    countryList.add(jsonObject.getString("name"));
                                    countryResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.spinner_list, countryList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                country.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(SignUpActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status="2";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }
    private void getStateList() {
        if(!stateList.isEmpty()){
            stateList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.post(API_LIST.all_state)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addBodyParameter("country_id",country_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                stateList.add("Select State");
                                stateResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("state_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    stateList.add(jsonObject.getString("name"));
                                    stateResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.spinner_list, stateList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                state.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(SignUpActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status="3";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {
        myStateview.showContent();
        if(retry_status.equalsIgnoreCase("1")){
            signup();
        }else if(retry_status.equalsIgnoreCase("2")){
            getCountryList();
        }else if(retry_status.equalsIgnoreCase("3")){
            getStateList();
        }else {
            getCityList();
        }

    }
}
