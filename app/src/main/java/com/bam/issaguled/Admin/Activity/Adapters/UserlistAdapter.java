package com.bam.issaguled.Admin.Activity.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.Admin.Activity.responser.UserListResponser;
import com.bam.issaguled.R;

import java.util.List;

public class UserlistAdapter extends RecyclerView.Adapter<UserlistAdapter.UserlistViewHolder> {
    private Context mCtx;
    //we are storing all the products in a list
    List<UserListResponser.Data.User> userlist;
    private AdapterView.OnItemClickListener onItemClickListener;
    private boolean isSelectedAll;
    public ItemListner itemListner;


    //getting the context and product list with constructor
    public UserlistAdapter(Context mCtx, List<UserListResponser.Data.User> userlist, ItemListner itemListner) {
        this.mCtx = mCtx;
        this.userlist = userlist;
        this.itemListner = itemListner;
        this.onItemClickListener = onItemClickListener;

    }

    @Override
    public UserlistAdapter.UserlistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.user_list, null);
        return new UserlistAdapter.UserlistViewHolder(view, onItemClickListener);
    }

    public void onBindViewHolder(UserlistAdapter.UserlistViewHolder holder, int position) {
        //getting the product of the specified position
        if (!isSelectedAll) {
            holder.checkBox.setChecked(false);
            itemListner.onClick(userlist.get(holder.getAdapterPosition()).getUserId().toString(), holder.getAdapterPosition(),
                    "2",userlist.get(holder.getAdapterPosition()).getEmail());
        } else {
            holder.checkBox.setChecked(true);
            itemListner.onClick(userlist.get(holder.getAdapterPosition()).getUserId().toString(),
                    holder.getAdapterPosition(), "1",userlist.get(holder.getAdapterPosition()).getEmail());
        }
        holder.name.setText(userlist.get(position).getName());
        holder.number.setText(userlist.get(position).getEmail());
        holder.city.setText(userlist.get(position).getCity());
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CompoundButton) v).isChecked()) {
                    itemListner.onClick(userlist.get(holder.getAdapterPosition()).getUserId().toString(),
                            holder.getAdapterPosition(), "1",userlist.get(holder.getAdapterPosition()).getEmail());
                } else {
                    itemListner.onClick(userlist.get(holder.getAdapterPosition()).getUserId().toString(), holder.getAdapterPosition(),
                            "3",userlist.get(holder.getAdapterPosition()).getEmail());
                }
            }
        });
        // holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(orders.getImage()));
    }

    public void selectAll() {
        isSelectedAll = true;
        notifyDataSetChanged();
    }

    public void unselectall() {
        isSelectedAll = false;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return userlist.size();
    }

    class UserlistViewHolder extends RecyclerView.ViewHolder {
        TextView name, number, city;
        CheckBox checkBox;

        public UserlistViewHolder(View itemView, AdapterView.OnItemClickListener onItemClickListener) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);
            city = itemView.findViewById(R.id.city);
            checkBox = itemView.findViewById(R.id.imageView2);
        }
    }

    public interface ItemListner {
        void onClick(String user_id, int index, String type,String email);
    }
}

