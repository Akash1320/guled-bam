package com.bam.issaguled.Admin.Activity.activity;

public class UserDonation {
    private int id;
    private String name;
    private String number;
    private String city;
    private String amount;

    public UserDonation(int id, String name, String number, String city, String amount){

        this.id=id;
        this.name=name;
        this.city=city;
        this.number=number;
        this.amount=amount;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
