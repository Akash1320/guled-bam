package com.bam.issaguled.Admin.Activity.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.Admin.Activity.responser.DonationDetailsResponser;
import com.bam.issaguled.R;

import java.util.List;

public class DonationDetailsAdapter extends RecyclerView.Adapter<DonationDetailsAdapter.UserDonationViewHolder> {
    private Context mCtx;
    //we are storing all the products in a list
    List<DonationDetailsResponser.Data.Donation> donation;

    //getting the context and product list with constructor
    public DonationDetailsAdapter(Context mCtx,  List<DonationDetailsResponser.Data.Donation> donation) {
        this.mCtx = mCtx;
        this.donation = donation;

    }


    @Override
    public DonationDetailsAdapter.UserDonationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
       View view = LayoutInflater.from(mCtx).inflate(R.layout.single_row_donationdetails,parent,false);
       return new UserDonationViewHolder(view);
    }

    public void onBindViewHolder(DonationDetailsAdapter.UserDonationViewHolder holder, int position) {

        holder.tvDate.setText(donation.get(position).getDatetime());
        holder.tvDonation.setText("Donation : "+donation.get(position).getAmount());
    }


    @Override
    public int getItemCount() {
        return donation.size();
    }

    class UserDonationViewHolder extends RecyclerView.ViewHolder {
        TextView tvReferenceID, tvDate, tvDonation;

        public UserDonationViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvDonation = itemView.findViewById(R.id.tvDonation);
        }
    }
}

