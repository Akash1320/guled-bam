package com.bam.issaguled.Admin.Activity.responser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserListResponser {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("user")
        @Expose
        private List<User> user = null;
        @SerializedName("last_donation")
        @Expose
        private Integer lastDonation;
        @SerializedName("register_users")
        @Expose
        private Integer registerUsers;
        @SerializedName("total_donation")
        @Expose
        private Integer totalDonation;

        public List<User> getUser() {
            return user;
        }

        public void setUser(List<User> user) {
            this.user = user;
        }

        public Integer getLastDonation() {
            return lastDonation;
        }

        public void setLastDonation(Integer lastDonation) {
            this.lastDonation = lastDonation;
        }

        public Integer getRegisterUsers() {
            return registerUsers;
        }

        public void setRegisterUsers(Integer registerUsers) {
            this.registerUsers = registerUsers;
        }

        public Integer getTotalDonation() {
            return totalDonation;
        }

        public void setTotalDonation(Integer totalDonation) {
            this.totalDonation = totalDonation;
        }
        public class User {

            @SerializedName("user_id")
            @Expose
            private Integer userId;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("city")
            @Expose
            private String city;
            @SerializedName("country")
            @Expose
            private String country;
            @SerializedName("donation")
            @Expose
            private Integer donation;

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public Integer getDonation() {
                return donation;
            }

            public void setDonation(Integer donation) {
                this.donation = donation;
            }

        }
    }
}
