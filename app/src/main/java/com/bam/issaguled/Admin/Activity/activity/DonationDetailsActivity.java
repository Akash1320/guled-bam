package com.bam.issaguled.Admin.Activity.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bam.issaguled.Admin.Activity.Adapters.DonationDetailsAdapter;
import com.bam.issaguled.Admin.Activity.responser.DonationDetailsResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import static com.bam.issaguled.other.API_LIST.user_donation_details;

public class DonationDetailsActivity extends AppCompatActivity implements ProgressClickListener {

    private TextView tvName;
    private TextView tvTotalDonation;
    private TextView tvEmail;
    private TextView tvCity;
    private TextView tvMobileNumber;
    private RecyclerView recyclerView;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private String user_id;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_details);

        if (getIntent() != null) {
            user_id = getIntent().getStringExtra("user_id");
        }

        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        toolbar= findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvName = findViewById(R.id.tvName);
        tvMobileNumber = findViewById(R.id.tvMobileNumber);
        tvTotalDonation = findViewById(R.id.tvTotalDonation);
        tvEmail = findViewById(R.id.tvEmail);
        tvCity = findViewById(R.id.tvCity);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        if (Methods.isOnline(this)) {
            getDonationDetails();
        }
    }

    private void getDonationDetails() {
        myStateview.showLoading();
        AndroidNetworking.post(user_donation_details)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        try {
                            Gson gson = new Gson();
                            DonationDetailsResponser donationDetailsResponser = gson.fromJson(response.toString(), DonationDetailsResponser.class);

                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                tvName.setText("Name : " + donationDetailsResponser.getData().getName());
                                tvEmail.setText("Email : " + donationDetailsResponser.getData().getEmail());
                                tvTotalDonation.setText("Total Donation : " + donationDetailsResponser.getData().getTotalDonation());
                                tvCity.setText("City : " + donationDetailsResponser.getData().getCity());
                                tvMobileNumber.setText("Mobile Number : " + donationDetailsResponser.getData().getMobileNumber());

                                DonationDetailsAdapter adapter = new DonationDetailsAdapter(DonationDetailsActivity.this, donationDetailsResponser.getData().getDonation());
                                recyclerView.setAdapter(adapter);


                            } else {
                                myStateview.showContent();
                                MyToast.display(DonationDetailsActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }
}
