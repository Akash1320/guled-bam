package com.bam.issaguled.Admin.Activity.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.Admin.Activity.Adapters.SecurityAdapter;
import com.bam.issaguled.Admin.Activity.responser.AgendaDetailsResponser;
import com.bam.issaguled.Admin.Activity.responser.InvitedUserResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.SignInActivity;
import com.bam.issaguled.UserProfileActivity;
import com.bam.issaguled.other.API_LIST;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.TwitterCore;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;
import static com.bam.issaguled.other.API_LIST.event_details;
import static com.bam.issaguled.other.API_LIST.event_invited_users;

public class AgendaDetailsActicity extends AppCompatActivity implements ProgressClickListener {
    RecyclerView recyclerView;
    ProgressBar progressBar;
    SecurityAdapter securityAdapter;
    private Toolbar toolbar;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private String agenda_id = "";
    private TextView textView40;
    private TextView tvHeader;
    private TextView yes;
    private TextView no;
    private LinearLayout llPercentage;
    private ImageView ivEdit;
    private ImageView ivDelete;
    private AgendaDetailsResponser agendaDetailsResponser;
    private AlertDialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_second2);

        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);
        if (getIntent() != null) {
            agenda_id = getIntent().getStringExtra("agenda_id");
            Log.e("agenda_id", ">>>>>>>>>>>>>>>>>" + agenda_id);
        }

        llPercentage = findViewById(R.id.llPercentage);
        progressBar = findViewById(R.id.progressBar1);
        no = findViewById(R.id.no);
        yes = findViewById(R.id.yes);
        tvHeader = findViewById(R.id.tvHeader);
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.security_recycler);
        textView40 = findViewById(R.id.textView40);
        ivEdit = findViewById(R.id.ivEdit);
        ivDelete = findViewById(R.id.ivDelete);
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(AgendaDetailsActicity.this);
                View mView1 = getLayoutInflater().inflate(R.layout.dialog_logout, null);
                TextView tvTitle = mView1.findViewById(R.id.tvTitle);
                TextView tvMessage = mView1.findViewById(R.id.tvMessage);
                tvTitle.setText("Remove");
                tvMessage.setText("Are you sure you want to Remove?");
                TextView TvExit = mView1.findViewById(R.id.TvExit);
                TextView TvClose = mView1.findViewById(R.id.TvClose);
                TvClose.setText("Remove");
                TvExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Methods.isOnline(AgendaDetailsActicity.this)) {
                            removeAgenda();
                        }
                    }
                });
                mBuilder.setView(mView1);
                dialog = mBuilder.create();
                dialog.show();
            }
        });
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AgendaDetailsActicity.this, CreateAgendaActivity.class)
                        .putExtra("userlist", "")
                        .putExtra("tag", "edit")
                        .putExtra("event_id", String.valueOf(agendaDetailsResponser.getData().getEventId()))
                        .putExtra("title", agendaDetailsResponser.getData().getTitle())
                        .putExtra("des", agendaDetailsResponser.getData().getDescription())
                        .putExtra("image", agendaDetailsResponser.getData().getIconImage())
                        .putExtra("bg", agendaDetailsResponser.getData().getBackgroundImage()));
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            textView40.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);

        if (Methods.isOnline(this)) {
            getDetails();
            getUserList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Methods.isOnline(this)) {
            getDetails();
            getUserList();
        }
    }

    private void getDetails() {
        myStateview.showLoading();
        AndroidNetworking.post(event_details)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("agenda_id", agenda_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        agendaDetailsResponser = gson.fromJson(response.toString(), AgendaDetailsResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                textView40.setText(agendaDetailsResponser.getData().getDescription());
                                tvHeader.setText(agendaDetailsResponser.getData().getTitle());
                                /*                     float attendee =agendaDetailsResponser.getData().getAttendeesUsers();
                                Log.e("invited",">>>>>>>>>>>>"+invited);
                                Log.e("attendee",">>>>>>>>>>>>"+attendee);
                                float progress= attendee / invited;
                                Log.e("progress",">>>>>>>>>>>>"+progress);
                                int p= (int) (progress*100);
                                progressBar.setProgress(p);
                                yes.setText(new DecimalFormat("##.#").format(progress*100)+"%");
                                no.setText(new DecimalFormat("##.#").format(100-progress*100)+"%");*/
                                float accepted = Integer.parseInt(response.getJSONObject("data").getString("attendees_users"));
                                float rejected = Integer.parseInt(response.getJSONObject("data").getString("not_attendees_users"));

                                float total_count = accepted + rejected;
                                Log.e("accepted", ">>>>>>>>>>>>>>>>>>>>>>>" + accepted);
                                Log.e("rejected", ">>>>>>>>>>>>>>>>>>>>>>>" + rejected);
                                Log.e("total_count", ">>>>>>>>>>>>>>>>>>>>>>>" + total_count);

                                if (total_count != 0) {
                                    llPercentage.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.VISIBLE);
                                    if (accepted != 0.0) {
                                        float percent = accepted / total_count;
                                        Log.e("percent", ">>>>>>>>>>>>>>>>>>>>>>>" + percent);
                                        yes.setText(new DecimalFormat("##.#").format(percent * 100) + "%");
                                        no.setText(new DecimalFormat("##.#").format(100 - percent * 100) + "%");
                                        int p = (int) (percent * 100);
                                        progressBar.setProgress(p);
                                    } else {
                                        float percent = rejected / total_count;
                                        Log.e("percent", ">>>>>>>>>>>>>>>>>>>>>>>" + percent);
                                        no.setText(new DecimalFormat("##.#").format(percent * 100) + "%");
                                        yes.setText(new DecimalFormat("##.#").format(100 - percent * 100) + "%");
                                        if (percent == 1.0) {
                                            progressBar.setProgress(0);
                                        } else {
                                            int p = (int) (percent * 100);
                                            progressBar.setProgress(p);
                                        }
                                    }
                                } else {
                                    llPercentage.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                }

                               /* securityAdapter = new SecurityAdapter(AgendaDetailsActicity.this, agendaDetailsResponser.getData());
                                recyclerView.setAdapter(securityAdapter);*/

                            } else {
                                myStateview.showContent();
                                MyToast.display(AgendaDetailsActicity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void getUserList() {
        myStateview.showLoading();
        AndroidNetworking.post(event_invited_users)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("agenda_id", agenda_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        InvitedUserResponser invitedUserResponser = gson.fromJson(response.toString(), InvitedUserResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                securityAdapter = new SecurityAdapter(AgendaDetailsActicity.this, invitedUserResponser.getData().getInvitedUsers());
                                recyclerView.setAdapter(securityAdapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(AgendaDetailsActicity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void removeAgenda() {
        myStateview.showLoading();
        AndroidNetworking.post(API_LIST.agendaDelete)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("event_id", agenda_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                MyToast.display(AgendaDetailsActicity.this, response.getString("message"));
                                finish();

                            } else {
                                myStateview.showContent();
                                MyToast.display(AgendaDetailsActicity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }
}
