package com.bam.issaguled.Admin.Activity;

import android.content.Intent;
import android.os.Bundle;

import com.bam.issaguled.Admin.Activity.Adapters.AgendaAdapter;
import com.bam.issaguled.Admin.Activity.activity.NotificationAdminActivity;
import com.bam.issaguled.Admin.Activity.activity.CreateAgendaActivity;
import com.bam.issaguled.Admin.Activity.activity.SettingActivity;
import com.bam.issaguled.Admin.Activity.responser.AgendaResponser;
import com.bam.issaguled.SignInActivity;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bam.issaguled.R;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import static com.bam.issaguled.other.API_LIST.get_all_events;

public class AdminHomeActivity extends AppCompatActivity implements ProgressClickListener {
    public static TextView title;
    ViewPager viewPager;
    TabLayout tabLayout;
    public static ImageView back;
    public FloatingActionButton add;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private RecyclerView recyclerView;
    private AgendaResponser agendaResponser;
    private TextView tvRegisterUser;
    private TextView tvTotalDonation;
    private TextView tvLastDonation;
    private ImageView ivNotification;
    private ImageView ivProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);
        add = findViewById(R.id.floatingActionButton);
        ivNotification = findViewById(R.id.ivNotification);
        ivProfile = findViewById(R.id.ivProfile);
        title = (TextView) findViewById(R.id.toolbar_title);
        recyclerView = findViewById(R.id.recyclerview);
        tvRegisterUser = findViewById(R.id.tvRegisterUser);
        tvTotalDonation = findViewById(R.id.tvTotalDonation);
        tvLastDonation = findViewById(R.id.tvLastDonation);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setNestedScrollingEnabled(false);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminHomeActivity.this, CreateAgendaActivity.class)
                        .putExtra("userlist", "")
                        .putExtra("tag", ""));
                overridePendingTransition(0, 0);
            }
        });
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminHomeActivity.this, SettingActivity.class));
                overridePendingTransition(0, 0);
            }
        });
        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminHomeActivity.this, NotificationAdminActivity.class));
                overridePendingTransition(0, 0);
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Methods.isOnline(this)) {
            getAgendasList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Methods.isOnline(this)) {
            getAgendasList();
        }
    }

    private void getAgendasList() {
        myStateview.showLoading();
        Log.e("token", ">>>>>>>>>>>>>>" + "Bearer" + myPreferences.getToken());
        AndroidNetworking.get(get_all_events)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        Gson gson = new Gson();
                        agendaResponser = gson.fromJson(response.toString(), AgendaResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                tvRegisterUser.setText("" + agendaResponser.getData().getRegisterUsers());
                                tvTotalDonation.setText("$" + agendaResponser.getData().getTotalDonation());
                                tvLastDonation.setText("$" + agendaResponser.getData().getLastDonation());
                                AgendaAdapter adapter = new AgendaAdapter(AdminHomeActivity.this, agendaResponser.getData().getEvents());
                                recyclerView.setAdapter(adapter);
                            } else if (response.getString("success").equalsIgnoreCase("404")) {
                                MyToast.display(AdminHomeActivity.this, response.getString("message"));
                                myPreferences.clearPreferences();
                                startActivity(new Intent(AdminHomeActivity.this, SignInActivity.class));
                                finishAffinity();
                            }
                            else {
                                myStateview.showContent();
                                MyToast.display(AdminHomeActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    @Override
    public void onRetryClick() {
        if (Methods.isOnline(this)) {
            getAgendasList();
        }
    }
}
