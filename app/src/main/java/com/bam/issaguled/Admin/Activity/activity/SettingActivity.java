package com.bam.issaguled.Admin.Activity.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bam.issaguled.Admin.Activity.responser.InvitedUserResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.SignInActivity;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import static com.bam.issaguled.other.API_LIST.setting_page_contain;

public class SettingActivity extends AppCompatActivity implements ProgressClickListener {

    Button logout;
    RelativeLayout users, profile, donation;
    private Toolbar toolbar;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private TextView tvRegisteredUser;
    private TextView tvDonation;
    private TextView tvLastDonation;
    private AlertDialog dialog;
    private ImageView ivLogout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_setting);
        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        toolbar = findViewById(R.id.toolbar);
        ivLogout = findViewById(R.id.ivLogout);
        tvLastDonation = findViewById(R.id.tvLastDonation);
        tvRegisteredUser = findViewById(R.id.tvRegisteredUser);
        tvDonation = findViewById(R.id.tvDonation);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        donation = findViewById(R.id.donantion);
        profile = findViewById(R.id.my_profile);
        users = findViewById(R.id.users);
        logout = findViewById(R.id.logout);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, ProfileAdminActivity.class));
                overridePendingTransition(0, 0);
            }
        });
        users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, UserListActivity.class));
                overridePendingTransition(0, 0);
            }
        });
        donation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, DonationActivity.class));
                overridePendingTransition(0, 0);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(SettingActivity.this);
                View mView1 = getLayoutInflater().inflate(R.layout.dialog_logout, null);
                TextView TvExit = mView1.findViewById(R.id.TvExit);
                TextView TvClose = mView1.findViewById(R.id.TvClose);
                TvExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        myPreferences.clearPreferences();
                        myPreferences.setFirst_time("1");
                        MyToast.display(SettingActivity.this, "Logged Out");

                        startActivity(new Intent(SettingActivity.this, SignInActivity.class));
                        finishAffinity();
                    }
                });
                mBuilder.setView(mView1);
                dialog = mBuilder.create();
                dialog.show();
            }
        });

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(SettingActivity.this);
                View mView1 = getLayoutInflater().inflate(R.layout.dialog_logout, null);
                TextView TvExit = mView1.findViewById(R.id.TvExit);
                TextView TvClose = mView1.findViewById(R.id.TvClose);
                TvExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        myPreferences.clearPreferences();
                        myPreferences.setFirst_time("1");
                        MyToast.display(SettingActivity.this, "Logged Out");

                        startActivity(new Intent(SettingActivity.this, SignInActivity.class));
                        finishAffinity();
                    }
                });
                mBuilder.setView(mView1);
                dialog = mBuilder.create();
                dialog.show();
            }
        });
        if (Methods.isOnline(this)) {
            getData();
        }

    }

    private void getData() {
        myStateview.showLoading();
        AndroidNetworking.get(setting_page_contain)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        InvitedUserResponser invitedUserResponser = gson.fromJson(response.toString(), InvitedUserResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                tvDonation.setText("$" + response.getJSONObject("data").getString("total_donation"));
                                tvLastDonation.setText("$" + response.getJSONObject("data").getString("last_donation"));
                                tvRegisteredUser.setText("" + response.getJSONObject("data").getString("register_users"));

                            } else {
                                myStateview.showContent();
                                MyToast.display(SettingActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }
}
