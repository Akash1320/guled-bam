package com.bam.issaguled.Admin.Activity.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.Admin.Activity.responser.UserListResponser;
import com.bam.issaguled.R;

import java.util.List;

public class UserDonationAdapter extends RecyclerView.Adapter<UserDonationAdapter.UserDonationViewHolder> {
    private Context mCtx;
    //we are storing all the products in a list
    List<UserListResponser.Data.User> user;
    private AdapterView.OnItemClickListener onItemClickListener;

    //getting the context and product list with constructor
    public UserDonationAdapter(Context mCtx, List<UserListResponser.Data.User> user) {
        this.mCtx = mCtx;
        this.user = user;
        this.onItemClickListener = onItemClickListener;

    }
    @Override
    public UserDonationAdapter.UserDonationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.user_donation, null);
        return new UserDonationAdapter.UserDonationViewHolder(view, onItemClickListener);
    }

    public void onBindViewHolder(UserDonationAdapter.UserDonationViewHolder holder, int position) {
        //getting the product of the specified position

        holder.name.setText(user.get(position).getName());
        holder.number.setText(user.get(position).getEmail());
        holder.city.setText(user.get(position).getCity());
        holder.amount.setText(""+user.get(position).getDonation());
        // holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(orders.getImage()));
    }


    @Override
    public int getItemCount() {
        return user.size();
    }

    class UserDonationViewHolder extends RecyclerView.ViewHolder {
        TextView name, number, city, amount;
        CheckBox checkBox;

        public UserDonationViewHolder(View itemView, AdapterView.OnItemClickListener onItemClickListener) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);
            city = itemView.findViewById(R.id.city);
            checkBox = itemView.findViewById(R.id.imageView2);
            amount = itemView.findViewById(R.id.textView11);

        }
    }
}

