package com.bam.issaguled.Admin.Activity.responser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InvitedUserResponser {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("event_id")
        @Expose
        private Integer eventId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("created_by")
        @Expose
        private String createdBy;
        @SerializedName("donation")
        @Expose
        private Integer donation;
        @SerializedName("last_donation")
        @Expose
        private Integer lastDonation;
        @SerializedName("register_users")
        @Expose
        private Integer registerUsers;
        @SerializedName("total_donation")
        @Expose
        private Integer totalDonation;
        @SerializedName("invited_users")
        @Expose
        private List<InvitedUser> invitedUsers = null;

        public Integer getEventId() {
            return eventId;
        }

        public void setEventId(Integer eventId) {
            this.eventId = eventId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getDonation() {
            return donation;
        }

        public void setDonation(Integer donation) {
            this.donation = donation;
        }

        public Integer getLastDonation() {
            return lastDonation;
        }

        public void setLastDonation(Integer lastDonation) {
            this.lastDonation = lastDonation;
        }

        public Integer getRegisterUsers() {
            return registerUsers;
        }

        public void setRegisterUsers(Integer registerUsers) {
            this.registerUsers = registerUsers;
        }

        public Integer getTotalDonation() {
            return totalDonation;
        }

        public void setTotalDonation(Integer totalDonation) {
            this.totalDonation = totalDonation;
        }

        public List<InvitedUser> getInvitedUsers() {
            return invitedUsers;
        }

        public void setInvitedUsers(List<InvitedUser> invitedUsers) {
            this.invitedUsers = invitedUsers;
        }

        public class InvitedUser {

            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("country")
            @Expose
            private String country;
            @SerializedName("city")
            @Expose
            private String city;
            @SerializedName("user_donation_amount")
            @Expose
            private Integer userDonationAmount;
            @SerializedName("status")
            @Expose
            private String status;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public Integer getUserDonationAmount() {
                return userDonationAmount;
            }

            public void setUserDonationAmount(Integer userDonationAmount) {
                this.userDonationAmount = userDonationAmount;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

        }
    }
}
