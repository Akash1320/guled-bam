package com.bam.issaguled.Admin.Activity.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bam.issaguled.R;
import com.bam.issaguled.other.API_LIST;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.ProgressClickListener;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangeasswordActivity extends AppCompatActivity implements ProgressClickListener {

    private Toolbar toolbar;
    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmNewPassword;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private Button btnChangePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changeassword);
        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);
        toolbar = findViewById(R.id.toolbar);
        btnChangePassword = findViewById(R.id.btnChangePassword);
        edtOldPassword = findViewById(R.id.edtOldPassword);
        edtNewPassword = findViewById(R.id.edtNewPassword);
        edtConfirmNewPassword = findViewById(R.id.edtConfirmNewPassword);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtOldPassword.getText().toString().isEmpty()) {
                    Toast.makeText(ChangeasswordActivity.this, "Please enter Old Password", Toast.LENGTH_SHORT).show();
                } else if (edtNewPassword.getText().toString().isEmpty()) {
                    Toast.makeText(ChangeasswordActivity.this, "Please enter New Password", Toast.LENGTH_SHORT).show();
                } else if (edtConfirmNewPassword.getText().toString().isEmpty()) {
                    Toast.makeText(ChangeasswordActivity.this, "Please enter Confirm New Password", Toast.LENGTH_SHORT).show();
                } else if (!edtNewPassword.getText().toString().equals(edtConfirmNewPassword.getText().toString())) {
                    Toast.makeText(ChangeasswordActivity.this, "New Password and Confirm Password should be same", Toast.LENGTH_SHORT).show();
                } else {
                    if (Methods.isOnline(ChangeasswordActivity.this)) {
                        changePassword();
                    }
                }
            }
        });
    }

    private void changePassword() {
        myStateview.showLoading();
        Log.e("access_token", ">>>>>>>>>>>>>>>>>>>>>" + myPreferences.getToken());
        Log.e("old_password", ">>>>>>>>>>>>>>>>>>>>>" + edtOldPassword.getText().toString());
        Log.e("new_password", ">>>>>>>>>>>>>>>>>>>>>" + edtNewPassword.getText().toString());
        AndroidNetworking.post("")
                .addBodyParameter("access_token", myPreferences.getToken())
                .addBodyParameter("old_password", edtOldPassword.getText().toString())
                .addBodyParameter("new_password", edtNewPassword.getText().toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("code").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                Toast.makeText(ChangeasswordActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                edtConfirmNewPassword.setText("");
                                edtNewPassword.setText("");
                                edtOldPassword.setText("");
                            } else if (response.getString("code").equalsIgnoreCase("202")) {
                               // MethodFactory.forceLogoutDialog(ChangeasswordActivity.this);

                            } else {
                                myStateview.showRetry();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }

    @Override
    public void onRetryClick() {
        if (Methods.isOnline(this)) {
            changePassword();
        }
    }
}
