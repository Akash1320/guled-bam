package com.bam.issaguled.Admin.Activity.responser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DonationListingResponser {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("donations")
        @Expose
        private List<Donation> donations = null;
        @SerializedName("last_donation")
        @Expose
        private String lastDonation;
        @SerializedName("register_users")
        @Expose
        private Integer registerUsers;
        @SerializedName("total_donation")
        @Expose
        private Integer totalDonation;

        public List<Donation> getDonations() {
            return donations;
        }

        public void setDonations(List<Donation> donations) {
            this.donations = donations;
        }

        public String getLastDonation() {
            return lastDonation;
        }

        public void setLastDonation(String lastDonation) {
            this.lastDonation = lastDonation;
        }

        public Integer getRegisterUsers() {
            return registerUsers;
        }

        public void setRegisterUsers(Integer registerUsers) {
            this.registerUsers = registerUsers;
        }

        public Integer getTotalDonation() {
            return totalDonation;
        }

        public void setTotalDonation(Integer totalDonation) {
            this.totalDonation = totalDonation;
        }
        public class Donation {

            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("mobile_number")
            @Expose
            private String mobileNumber;
            @SerializedName("amount")
            @Expose
            private String amount;
            @SerializedName("city")
            @Expose
            private String city;
            @SerializedName("pin_code")
            @Expose
            private Integer pinCode;
            @SerializedName("expiry_date")
            @Expose
            private String expiryDate;

            @SerializedName("user_register")
            @Expose
            private String userRegister;
            @SerializedName("user_id")
            @Expose
            private String user_id;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getMobileNumber() {
                return mobileNumber;
            }

            public void setMobileNumber(String mobileNumber) {
                this.mobileNumber = mobileNumber;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public Integer getPinCode() {
                return pinCode;
            }

            public void setPinCode(Integer pinCode) {
                this.pinCode = pinCode;
            }

            public String getExpiryDate() {
                return expiryDate;
            }

            public void setExpiryDate(String expiryDate) {
                this.expiryDate = expiryDate;
            }



            public String getUserRegister() {
                return userRegister;
            }

            public void setUserRegister(String userRegister) {
                this.userRegister = userRegister;
            }

        }
    }
}
