package com.bam.issaguled.Admin.Activity.responser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserAgendaResponser
{
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("event")
        @Expose
        private List<Event> event = null;
        @SerializedName("last_donation")
        @Expose
        private Integer lastDonation;
        @SerializedName("register_users")
        @Expose
        private Integer registerUsers;
        @SerializedName("total_donation")
        @Expose
        private Integer totalDonation;

        public List<Event> getEvent() {
            return event;
        }

        public void setEvent(List<Event> event) {
            this.event = event;
        }

        public Integer getLastDonation() {
            return lastDonation;
        }

        public void setLastDonation(Integer lastDonation) {
            this.lastDonation = lastDonation;
        }

        public Integer getRegisterUsers() {
            return registerUsers;
        }

        public void setRegisterUsers(Integer registerUsers) {
            this.registerUsers = registerUsers;
        }

        public Integer getTotalDonation() {
            return totalDonation;
        }

        public void setTotalDonation(Integer totalDonation) {
            this.totalDonation = totalDonation;
        }
        public class Event {

            @SerializedName("event_assign_user_id")
            @Expose
            private Integer eventAssignUserId;
            @SerializedName("event_id")
            @Expose
            private Integer eventId;
            @SerializedName("user_id")
            @Expose
            private Integer userId;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("background_image")
            @Expose
            private String backgroundImage;
            @SerializedName("icon_image")
            @Expose
            private String iconImage;
            @SerializedName("created_by")
            @Expose
            private String createdBy;
            @SerializedName("status")
            @Expose
            private String status;

            public Integer getEventAssignUserId() {
                return eventAssignUserId;
            }

            public void setEventAssignUserId(Integer eventAssignUserId) {
                this.eventAssignUserId = eventAssignUserId;
            }

            public Integer getEventId() {
                return eventId;
            }

            public void setEventId(Integer eventId) {
                this.eventId = eventId;
            }

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getBackgroundImage() {
                return backgroundImage;
            }

            public void setBackgroundImage(String backgroundImage) {
                this.backgroundImage = backgroundImage;
            }

            public String getIconImage() {
                return iconImage;
            }

            public void setIconImage(String iconImage) {
                this.iconImage = iconImage;
            }

            public String getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(String createdBy) {
                this.createdBy = createdBy;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

        }
    }

}
