package com.bam.issaguled.Admin.Activity.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.bam.issaguled.Admin.Activity.Adapters.DonationListAdapter;
import com.bam.issaguled.Admin.Activity.responser.DonationListingResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.get_all_donations;

public class DonationActivity extends AppCompatActivity implements ProgressClickListener {
    Spinner city, country;
    RecyclerView recyclerView;
    DonationListAdapter userDonationAdapter;
    private Toolbar toolbar;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private TextView tvRegisteredUser;
    private TextView tvTotalDonation;
    private TextView tvLastDonation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_donation);
        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        toolbar = findViewById(R.id.toolbar);
        tvLastDonation = findViewById(R.id.tvLastDonation);
        tvRegisteredUser = findViewById(R.id.tvRegisteredUser);
        tvTotalDonation = findViewById(R.id.tvTotalDonation);
        recyclerView = findViewById(R.id.recyclerview);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        country = (Spinner) findViewById(R.id.country);
        city = (Spinner) findViewById(R.id.city);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);

        if(Methods.isOnline(this)){
            getDonationList();
        }
    }

    private void getDonationList() {
        myStateview.showLoading();
        AndroidNetworking.get(get_all_donations)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization",  "Bearer" + myPreferences.getToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson= new Gson();
                        DonationListingResponser userListResponser= gson.fromJson(response.toString(),DonationListingResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                tvLastDonation.setText("$"+userListResponser.getData().getLastDonation());
                                tvRegisteredUser.setText(""+userListResponser.getData().getRegisterUsers());
                                tvTotalDonation.setText("$"+userListResponser.getData().getTotalDonation());
                                userDonationAdapter = new DonationListAdapter(DonationActivity.this, userListResponser.getData().getDonations());
                                recyclerView.setAdapter(userDonationAdapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(DonationActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }
}
