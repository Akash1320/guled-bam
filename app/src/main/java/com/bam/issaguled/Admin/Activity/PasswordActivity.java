package com.bam.issaguled.Admin.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.bam.issaguled.R;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import static com.bam.issaguled.other.API_LIST.admin_signIN;

public class PasswordActivity extends AppCompatActivity implements ProgressClickListener {
    Button submit;
    TextInputLayout passwordlayout;
    TextInputEditText password;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        token = FirebaseInstanceId.getInstance().getToken();
        Log.e("device_token",">>>>>>>>>>>>>>>>>>>>>"+token);
        myPreferences=new MyPreferences(this);
        myStateview= new MyStateview(this,null);

        submit = (Button) findViewById(R.id.set_password);
        passwordlayout = (TextInputLayout) findViewById(R.id.passwordlayout);
        password = (TextInputEditText) findViewById(R.id.password);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  Password = password.getText().toString().trim();
                Methods.hideKeyboardFrom(PasswordActivity.this,v);
                if(TextUtils.isEmpty(Password)){
                    MyToast.display(PasswordActivity.this,"Please Enter Password");
                }
                else {
                    if(Methods.isOnline(PasswordActivity.this)){
                        signin();
                    }

                }
            }
        });
    }

    private void signin() {
        myStateview.showLoading();
        AndroidNetworking.post(admin_signIN)
                .addHeaders("Content-Type","application/json")
                .addHeaders("Accept","application/json")
                .addBodyParameter("email", myPreferences.getEmail())
                .addBodyParameter("password", password.getText().toString())
                .addBodyParameter("deviceType", "android")
                .addBodyParameter("deviceToken", token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                myPreferences.setToken(response.getString("access_token"));
                                myPreferences.setId(response.getJSONObject("data").getString("user_id"));
                                myPreferences.setEmail(response.getJSONObject("data").getString("email"));
                                myPreferences.setUser_name(response.getJSONObject("data").getString("name"));
                                myPreferences.setCity(response.getJSONObject("data").getString("city_id"));
                                myPreferences.setCountry(response.getJSONObject("data").getString("country_id"));
                                myPreferences.setLogin("2");
                                Intent intent = new Intent(PasswordActivity.this, AdminHomeActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            } else {
                                myStateview.showContent();
                                MyToast.display(PasswordActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    @Override
    public void onRetryClick() {

    }
}
