package com.bam.issaguled.Admin.Activity.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.bam.issaguled.Admin.Activity.Adapters.UserDonationAdapter;
import com.bam.issaguled.Admin.Activity.responser.SelectAgendaResponser;
import com.bam.issaguled.Admin.Activity.responser.UserListResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.all_cities;
import static com.bam.issaguled.other.API_LIST.all_countries;
import static com.bam.issaguled.other.API_LIST.get_all_users;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserListActivity extends AppCompatActivity implements ProgressClickListener {
    Spinner city, country;
    FloatingActionButton add;
    RecyclerView recyclerview;

    UserDonationAdapter userDonationAdapter;
    String[] City_array = new String[]{
            "Delhi",
            "Mumbai",
            "Banglore",
            "Noida",
    };

    String[] Country_array = new String[]{
            "India",
            "America",
            "Austraila",
    };

    private Toolbar toolbar;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private String country_id;
    private String city_id="";
    private ArrayList<String> countryList=new ArrayList<>();
    private ArrayList<String> cityList=new ArrayList<>();
    private ArrayList<SelectAgendaResponser> countryResponserArrayList;
    private ArrayList<SelectAgendaResponser> cityResponserArrayList;
    private TextView tvRegisteredUser;
    private TextView tvTotalDonation;
    private TextView tvLastDonation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_user_list);

        myPreferences= new MyPreferences(this);
        myStateview= new MyStateview(this,null);
        /*country_id=myPreferences.getCountry();
        city_id= myPreferences.getCity();*/

        toolbar = findViewById(R.id.toolbar);
        tvLastDonation = findViewById(R.id.tvLastDonation);
        tvRegisteredUser = findViewById(R.id.tvRegisteredUser);
        tvTotalDonation = findViewById(R.id.tvTotalDonation);
        recyclerview = findViewById(R.id.recyclerview);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        add = findViewById(R.id.add);
        country = (Spinner) findViewById(R.id.country);
        city = (Spinner) findViewById(R.id.city);

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setNestedScrollingEnabled(false);
        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    country_id = countryResponserArrayList.get(position - 1).getId();
                    Log.e("country_id", "-----------" + country_id);
                    getUserList();
                    getCityList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                city_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    city_id = cityResponserArrayList.get(position - 1).getId();
                    Log.e("city_id", "-----------" + city_id);
                    getUserList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
   /*     userDonationAdapter = new UserDonationAdapter(this, userDonationList);
        recyclerview.setAdapter(userDonationAdapter);*/

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserListActivity.this, AddUserActivity.class));
                overridePendingTransition(0, 0);
            }
        });
        if(Methods.isOnline(this)){
            getUserList();
            getCountryList();
          //  getCityList();
        }
    }

    private void getUserList() {
        myStateview.showLoading();
        Log.e("country_id",">>>>>"+country_id);
        Log.e("city_id",">>>>>"+city_id);
        AndroidNetworking.post(get_all_users)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization",  "Bearer" + myPreferences.getToken())
                .addBodyParameter("country_id",country_id)
                .addBodyParameter("city_id",city_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("user_response", ">>>>>>>>>>>" + response);
                        Gson gson= new Gson();
                        UserListResponser userListResponser= gson.fromJson(response.toString(),UserListResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                tvLastDonation.setText("$"+userListResponser.getData().getLastDonation());
                                tvRegisteredUser.setText(""+userListResponser.getData().getRegisterUsers());
                                tvTotalDonation.setText("$"+userListResponser.getData().getTotalDonation());
                                userDonationAdapter = new UserDonationAdapter(UserListActivity.this, userListResponser.getData().getUser());
                                recyclerview.setAdapter(userDonationAdapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(UserListActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }
    private void getCountryList() {
        myStateview.showLoading();
        AndroidNetworking.get(all_countries)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                countryList.add("Select Country");
                                countryResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("country_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    countryList.add(jsonObject.getString("name"));
                                    countryResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserListActivity.this, R.layout.spinner_list, countryList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                country.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(UserListActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }



    private void getCityList() {
        if(!cityList.isEmpty()){
            cityList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.post(all_cities)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addBodyParameter("country_id",country_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                cityList.add("Select City");
                                cityResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("city_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    cityList.add(jsonObject.getString("name"));
                                    cityResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserListActivity.this, R.layout.spinner_list, cityList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                city.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(UserListActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }
}
