package com.bam.issaguled.Admin.Activity.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.Admin.Activity.Adapters.UserlistAdapter;
import com.bam.issaguled.Admin.Activity.responser.SelectAgendaResponser;
import com.bam.issaguled.Admin.Activity.responser.UserListResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.all_cities;
import static com.bam.issaguled.other.API_LIST.all_countries;
import static com.bam.issaguled.other.API_LIST.get_all_users;

public class SelectuserActivity extends AppCompatActivity implements ProgressClickListener, UserlistAdapter.ItemListner {

    RecyclerView recyclerView;
    UserlistAdapter userlistAdapter;
    Spinner country, city;
    private Toolbar toolbar;
    CheckBox checkAll;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private String country_id;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayList<String> cityList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> countryResponserArrayList;
    private ArrayList<SelectAgendaResponser> cityResponserArrayList;
    private ArrayList<String> selectuserlist;    private String city_id;

    private TextView tvDone;
    private String tag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_selectuser);

            selectuserlist = new ArrayList<>();
            myPreferences = new MyPreferences(this);
            myStateview = new MyStateview(this, null);
            country = (Spinner) findViewById(R.id.country);
            city = (Spinner) findViewById(R.id.city);
            tvDone = findViewById(R.id.tvDone);
            checkAll = findViewById(R.id.checkAll);
            toolbar = findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            recyclerView = findViewById(R.id.recyclerview);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(SelectuserActivity.this));

            country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    country_id = "";
                    if (position > 0) {
                        // routeName = parent.getItemAtPosition(position).toString();
                        country_id = countryResponserArrayList.get(position - 1).getId();
                        Log.e("country_id", "-----------" + country_id);
                        getUserList();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    city_id = "";
                    if (position > 0) {
                        // routeName = parent.getItemAtPosition(position).toString();
                        city_id = cityResponserArrayList.get(position - 1).getId();
                        Log.e("city_id", "-----------" + city_id);
                        getUserList();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            checkAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkAll.isChecked()) {
                        userlistAdapter.selectAll();
                    } else {
                        userlistAdapter.unselectall();
                    }
                }
            });

       /* ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(SelectuserActivity.this, R.layout.spinner_list, Country_array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country.setAdapter(adapter);*/
            if (Methods.isOnline(this)) {
                getUserList();
                getCountryList();
                getCityList();
            }

            tvDone.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View v) {

                    Intent i = new Intent();
                    i.putExtra("userlist", selectuserlist);
                    setResult(1,i);
                    finish();
                   /* startActivity(new Intent(SelectuserActivity.this,CreateAgendaActivity.class)
                    .putExtra("userlist",selectuserlist)
                    .putExtra("tag","1"));*/
                }
            });
        }


    private void getUserList() {
        myStateview.showLoading();
        AndroidNetworking.post(get_all_users)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("country_id", country_id)
                .addBodyParameter("city_id", city_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        UserListResponser userListResponser = gson.fromJson(response.toString(), UserListResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                JSONArray jsonArray = response.getJSONObject("data").getJSONArray("user");

                               /* for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    selectuserlist.add("[" + i + "]" + "[user_id]:" + jsonObject.getString("user_id"));
                                }
                                Log.e("selectuserlist",">>>>>>>>>>>>>>>>>" + selectuserlist);*/

                                userlistAdapter = new UserlistAdapter(SelectuserActivity.this,
                                        userListResponser.getData().getUser(),SelectuserActivity.this);
                                recyclerView.setAdapter(userlistAdapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(SelectuserActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    private void getCountryList() {
        myStateview.showLoading();
        AndroidNetworking.get(all_countries)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                countryList.add("Select Country");
                                countryResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("country_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    countryList.add(jsonObject.getString("name"));
                                    countryResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectuserActivity.this, R.layout.spinner_list, countryList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                country.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(SelectuserActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }

    private void getCityList() {
        myStateview.showLoading();
        AndroidNetworking.post(all_cities)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                cityList.add("Select City");
                                cityResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("city_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    cityList.add(jsonObject.getString("name"));
                                    cityResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectuserActivity.this, R.layout.spinner_list, cityList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                city.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(SelectuserActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }

    @Override
    public void onClick(String user_id, int index,String type,String email) {
        if(type.equalsIgnoreCase("1")) {
            if(!selectuserlist.contains(user_id)){
                selectuserlist.add(user_id);
                Log.e("selectuserlist", ">>>>>>>>>>>>>>>>>>" + selectuserlist);
            }
        }else if(type.equalsIgnoreCase("2")) {
            selectuserlist.clear();
            Log.e("selectuserlist", ">>>>>>>>>>>>>>>>>>" + selectuserlist);
        }else {
            Log.e("user_id", ">>>>>>>>>>>>>>>>>>" + user_id);
            int position= selectuserlist.indexOf(user_id);
            Log.e("position", ">>>>>>>>>>>>>>>>>>" + position);
            selectuserlist.remove(position);
            Log.e("selectuserlist", ">>>>>>>>>>>>>>>>>>" + selectuserlist);
        }

    }
}
