package com.bam.issaguled.Admin.Activity.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bam.issaguled.AdapterClasses.NotificationAdapter;
import com.bam.issaguled.NotificationResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.SignInActivity;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.admin_notification;


public class NotificationAdminActivity extends AppCompatActivity implements ProgressClickListener {
    RecyclerView recyclerView;
    private Toolbar toolbar;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private NotificationAdapter notificationAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notification_admin);
        myPreferences= new MyPreferences(this);
        myStateview= new MyStateview(this,null);

        recyclerView = findViewById(R.id.recyclerview);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(Methods.isOnline(this)){
            getNotifications();
        }

    }
    private void getNotifications() {
        myStateview.showLoading();
        AndroidNetworking.get(admin_notification)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        NotificationResponser notificationResponser = gson.fromJson(response.toString(), NotificationResponser.class);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                notificationAdapter = new NotificationAdapter(NotificationAdminActivity.this, notificationResponser.getData().getNotification());
                                recyclerView.setAdapter(notificationAdapter);

                            } else if(response.getString("success").equalsIgnoreCase("404")){
                                MyToast.display(NotificationAdminActivity.this, response.getString("message"));
                                myPreferences.clearPreferences();
                                startActivity(new Intent(NotificationAdminActivity.this, SignInActivity.class));
                                finishAffinity();
                            }else {
                                myStateview.showContent();
                                MyToast.display(NotificationAdminActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());
                    }
                });
    }

    @Override
    public void onRetryClick() {
        if(Methods.isOnline(this)){
            getNotifications();
        }
    }
}
