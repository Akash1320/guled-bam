package com.bam.issaguled.Admin.Activity.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bam.issaguled.EditProfileActivity;
import com.bam.issaguled.R;
import com.bam.issaguled.UserProfileActivity;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import static com.bam.issaguled.other.API_LIST.edit_profile;

public class ProfileAdminActivity extends AppCompatActivity implements ProgressClickListener {
    private static final int PICK_IMAGE = 100;
    ImageView camera, image,ivProfile;
    Uri imageUri;
    private Toolbar toolbar;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private TextView tvRegisteredUser;
    private TextView tvTotalDonation;
    private TextView tvLastDonation;
    private TextView tvName;
    private TextView tvEmail;
    private TextView tvState;
    private TextView tvMobileNumber;
    private TextView tvCity;
    private TextView tvCountry;
    private ImageView ivEdit;
    private String name="";
    private String mobile="";
    private String city="";
    private String location="";
    private String state="";
    private String email="";
    private String country="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile_admin);
        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        tvName = findViewById(R.id.tvName);
        ivEdit = findViewById(R.id.ivEdit);
        tvState = findViewById(R.id.tvState);
        tvCountry = findViewById(R.id.tvCountry);
        tvCity = findViewById(R.id.tvCity);
        tvMobileNumber = findViewById(R.id.tvMobileNumber);
        tvEmail = findViewById(R.id.tvEmail);
        tvRegisteredUser = findViewById(R.id.tvRegisteredUser);
        tvTotalDonation = findViewById(R.id.tvTotalDonation);
        tvLastDonation = findViewById(R.id.tvLastDonation);

        image = findViewById(R.id.image);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (Methods.isOnline(this)) {
            getProfile();
        }

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileAdminActivity.this, EditProfileActivity.class)
                        .putExtra("name",name)
                        .putExtra("email",email)
                        .putExtra("mobile",mobile)
                        .putExtra("city",city)
                        .putExtra("country",country)
                        .putExtra("state",state)
                        .putExtra("tag","admin")
                );
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Methods.isOnline(this)) {
            getProfile();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            //TODO: action
            imageUri = data.getData();
            image.setImageURI(imageUri);
        }


    }


    private void getProfile() {
        myStateview.showLoading();
        AndroidNetworking.post(edit_profile)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("login_id", myPreferences.getId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                name=response.getJSONObject("data").getString("name");
                                email= response.getJSONObject("data").getString("email");
                                mobile= response.getJSONObject("data").getString("mobile_number");
                                city=response.getJSONObject("data").getString("city");
                                country=response.getJSONObject("data").getString("country");
                                state=response.getJSONObject("data").getString("state");

                                tvRegisteredUser.setText(""+response.getJSONObject("data").getString("register_users"));
                                tvTotalDonation.setText("$"+response.getJSONObject("data").getString("total_donation"));
                                tvLastDonation.setText("$"+response.getJSONObject("data").getString("last_donation"));
                                tvName.setText("Name : "+response.getJSONObject("data").getString("name"));
                                tvEmail.setText("Email : "+response.getJSONObject("data").getString("email"));
                                tvMobileNumber.setText("Mobile Number : "+response.getJSONObject("data").getString("mobile_number"));
                                tvCity.setText("City : "+response.getJSONObject("data").getString("city"));
                                tvCountry.setText("Country : "+response.getJSONObject("data").getString("country"));
                                tvState.setText("State : "+response.getJSONObject("data").getString("state"));


                            } else {
                                myStateview.showContent();
                                MyToast.display(ProfileAdminActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }
}
