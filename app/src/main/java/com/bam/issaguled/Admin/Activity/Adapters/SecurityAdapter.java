package com.bam.issaguled.Admin.Activity.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bam.issaguled.Admin.Activity.responser.InvitedUserResponser;
import com.bam.issaguled.R;

import java.util.List;

public class SecurityAdapter extends RecyclerView.Adapter<SecurityAdapter.SecurityViewHolder> {
    private Context mCtx;
    //we are storing all the products in a list
    List<InvitedUserResponser.Data.InvitedUser> securityList;
    private AdapterView.OnItemClickListener onItemClickListener;

    //getting the context and product list with constructor
    public SecurityAdapter(Context mCtx, List<InvitedUserResponser.Data.InvitedUser> securityList) {
        this.mCtx = mCtx;
        this.securityList = securityList;
        this.onItemClickListener = onItemClickListener;

    }


    @Override
    public SecurityAdapter.SecurityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.votes, null);
        return new SecurityAdapter.SecurityViewHolder(view, onItemClickListener);
    }

    public void onBindViewHolder(SecurityViewHolder holder, int position) {
        //getting the product of the specified position
        holder.name.setText(securityList.get(position).getName());
        holder.number.setText(securityList.get(position).getEmail());
        holder.city.setText(securityList.get(position).getCity());
        String status = securityList.get(position).getStatus();
        // holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(orders.getImage()));
        if (status.equalsIgnoreCase("Accepted")) {
            holder.imageView.setBackgroundResource(R.drawable.yes_active);
        } else if (status.contains("Pending")) {
            holder.imageView.setBackgroundResource(R.drawable.pending);
        } else {
            holder.imageView.setBackgroundResource(R.drawable.no_active);
        }
    }


    @Override
    public int getItemCount() {
        return securityList.size();
    }

    class SecurityViewHolder extends RecyclerView.ViewHolder {
        TextView name, number, city;
        ImageView imageView;

        public SecurityViewHolder(View itemView, AdapterView.OnItemClickListener onItemClickListener) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);
            city = itemView.findViewById(R.id.city);
            imageView = itemView.findViewById(R.id.imageView2);

        }
    }
}

