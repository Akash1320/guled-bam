package com.bam.issaguled.Admin.Activity.responser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgendaDetailsResponser {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("event_id")
        @Expose
        private Integer eventId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("background_image")
        @Expose
        private String backgroundImage;
        @SerializedName("icon_image")
        @Expose
        private String iconImage;
        @SerializedName("created_by")
        @Expose
        private String createdBy;
        @SerializedName("invited_users")
        @Expose
        private Integer invitedUsers;
        @SerializedName("attendees_users")
        @Expose
        private Integer attendeesUsers;
        @SerializedName("donation")
        @Expose
        private Integer donation;
        @SerializedName("last_donation")
        @Expose
        private Integer lastDonation;
        @SerializedName("register_users")
        @Expose
        private Integer registerUsers;
        @SerializedName("total_donation")
        @Expose
        private Integer totalDonation;

        public Integer getEventId() {
            return eventId;
        }

        public void setEventId(Integer eventId) {
            this.eventId = eventId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getBackgroundImage() {
            return backgroundImage;
        }

        public void setBackgroundImage(String backgroundImage) {
            this.backgroundImage = backgroundImage;
        }

        public String getIconImage() {
            return iconImage;
        }

        public void setIconImage(String iconImage) {
            this.iconImage = iconImage;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getInvitedUsers() {
            return invitedUsers;
        }

        public void setInvitedUsers(Integer invitedUsers) {
            this.invitedUsers = invitedUsers;
        }

        public Integer getAttendeesUsers() {
            return attendeesUsers;
        }

        public void setAttendeesUsers(Integer attendeesUsers) {
            this.attendeesUsers = attendeesUsers;
        }

        public Integer getDonation() {
            return donation;
        }

        public void setDonation(Integer donation) {
            this.donation = donation;
        }

        public Integer getLastDonation() {
            return lastDonation;
        }

        public void setLastDonation(Integer lastDonation) {
            this.lastDonation = lastDonation;
        }

        public Integer getRegisterUsers() {
            return registerUsers;
        }

        public void setRegisterUsers(Integer registerUsers) {
            this.registerUsers = registerUsers;
        }

        public Integer getTotalDonation() {
            return totalDonation;
        }

        public void setTotalDonation(Integer totalDonation) {
            this.totalDonation = totalDonation;
        }

    }
}
