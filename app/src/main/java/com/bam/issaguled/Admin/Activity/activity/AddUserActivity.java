package com.bam.issaguled.Admin.Activity.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bam.issaguled.Admin.Activity.responser.SelectAgendaResponser;
import com.bam.issaguled.Admin.Activity.responser.UserListResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.SignUpActivity;
import com.bam.issaguled.other.API_LIST;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.bam.issaguled.other.API_LIST.all_cities;
import static com.bam.issaguled.other.API_LIST.all_countries;
import static com.bam.issaguled.other.API_LIST.user_create;

public class AddUserActivity extends AppCompatActivity implements ProgressClickListener {
    Spinner city, country,state;

    private Toolbar toolbar;
    private MyStateview myStateview;
    private MyPreferences myPreferences;
    private String country_id = "";
    private String city_id = "";
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayList<String> stateList = new ArrayList<>();
    private ArrayList<String> cityList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> stateResponserArrayList;
    private ArrayList<SelectAgendaResponser> countryResponserArrayList;
    private ArrayList<SelectAgendaResponser> cityResponserArrayList;
    private EditText name;
    private EditText email;
    private EditText number;
    private Button add;
    private String retry_status="";
    private String state_id="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_user);

        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);


        toolbar = findViewById(R.id.toolbar);
        state = findViewById(R.id.state);
        add = findViewById(R.id.add);
        number = findViewById(R.id.number);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        country = (Spinner) findViewById(R.id.country);
        city = (Spinner) findViewById(R.id.city);


        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    country_id = countryResponserArrayList.get(position - 1).getId();
                    Log.e("country_id", "-----------" + country_id);
                    getStateList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    state_id = stateResponserArrayList.get(position - 1).getId();
                    Log.e("state_id", "-----------" + state_id);
                    getCityList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                city_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    city_id = cityResponserArrayList.get(position - 1).getId();
                    Log.e("city_id", "-----------" + city_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().isEmpty()) {
                    MyToast.display(AddUserActivity.this, "Please Enter User Name");
                } else if (email.getText().toString().isEmpty()) {
                    MyToast.display(AddUserActivity.this, "Please Enter Email Address");
                } else if (number.getText().toString().isEmpty()) {
                    MyToast.display(AddUserActivity.this, "Please Enter Mobile Number");
                } else if (number.getText().toString().length() < 14) {
                    MyToast.display(AddUserActivity.this, "Please Enter Valid Mobile Number");
                } else if (country_id.isEmpty()) {
                    MyToast.display(AddUserActivity.this, "Please Select Country Name");
                } else if (state_id.isEmpty()) {
                    MyToast.display(AddUserActivity.this, "Please Select state Name");
                } else if (city_id.isEmpty()) {
                    MyToast.display(AddUserActivity.this, "Please Select City Name");
                } else {
                    if (Methods.isOnline(AddUserActivity.this)) {
                        addUser();
                    }
                }
            }
        });
        if (Methods.isOnline(this)) {
            getCountryList();
        }
    }

    private void addUser() {
        myStateview.showLoading();
        AndroidNetworking.post(user_create)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("name", name.getText().toString())
                .addBodyParameter("email", email.getText().toString())
                .addBodyParameter("mobile_number", number.getText().toString())
                .addBodyParameter("country_id", country_id)
                .addBodyParameter("city_id", city_id)
                .addBodyParameter("state_id", state_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        UserListResponser userListResponser = gson.fromJson(response.toString(), UserListResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                MyToast.display(AddUserActivity.this,response.getString("message"));
                                onBackPressed();

                            } else {
                                myStateview.showContent();
                                MyToast.display(AddUserActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getCountryList() {
        myStateview.showLoading();
        AndroidNetworking.get(all_countries)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                countryList.add("Select Country");
                                countryResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("country_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    countryList.add(jsonObject.getString("name"));
                                    countryResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddUserActivity.this, R.layout.spinner_list, countryList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                country.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(AddUserActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }
    private void getStateList() {
        if(!stateList.isEmpty()){
            stateList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.post(API_LIST.all_state)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addBodyParameter("country_id",country_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                stateList.add("Select State");
                                stateResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("state_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    stateList.add(jsonObject.getString("name"));
                                    stateResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddUserActivity.this, R.layout.spinner_list, stateList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                state.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(AddUserActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        retry_status="3";
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    private void getCityList() {
        if(!cityList.isEmpty()){
            cityList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.post(all_cities)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addBodyParameter("state_id", state_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONArray("data");
                                cityList.add("Select City");
                                cityResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("city_id"));
                                    countryResponser.setTitle(jsonObject.getString("name"));
                                    cityList.add(jsonObject.getString("name"));
                                    cityResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddUserActivity.this, R.layout.spinner_list, cityList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                city.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(AddUserActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }


    @Override
    public void onRetryClick() {

    }
}
