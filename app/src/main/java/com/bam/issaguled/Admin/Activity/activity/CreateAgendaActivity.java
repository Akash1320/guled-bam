package com.bam.issaguled.Admin.Activity.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bam.issaguled.Admin.Activity.responser.SelectAgendaResponser;
import com.bam.issaguled.Admin.Activity.responser.UserListResponser;
import com.bam.issaguled.R;
import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ImagePickerComponentHolder;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.features.imageloader.DefaultImageLoader;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.bam.issaguled.other.API_LIST.Base_url;
import static com.bam.issaguled.other.API_LIST.agenda_store;
import static com.bam.issaguled.other.API_LIST.agenda_subject_store;
import static com.bam.issaguled.other.API_LIST.agenda_update;
import static com.bam.issaguled.other.API_LIST.unassign_agenda_subject;

public class CreateAgendaActivity extends AppCompatActivity implements ProgressClickListener {
    ImageView add;
    TextView select_user;
    ArrayList<String> myList;
    Spinner spinner;
    String[] number = new String[]{
            "Smart Education",
            "Smart Economy",
            "Smart Security",
            "Smart Health",
    };
    private Toolbar toolbar;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private ArrayList<String> agendaList = new ArrayList<>();
    private ArrayList<SelectAgendaResponser> agendaResponserArrayList;
    private String event_id = "";
    private ImageView imageView4;
    private ImageView imageView5;
    private ImageView imageView3;
    private ImageView imageView6;
    private ImageView uploadICon;
    private ImageView ivBackground;
    private EditText edtDescription;
    private boolean icon_image;
    private boolean background_image;
    private File iconfile;
    private File backgroundfile;
    private String tag;
    private JSONArray jsonArray;
    private TextView submit;
    private String icon_string = "";
    private String bg_string = "";
    private String userlist = "";
    private TextView tvTitle;
    private TextView textView;
    private TextView textView5;
    private TextView textView6;
    private EditText edtTitle;
    private RelativeLayout relativeLayout3;
    private String title="";
    private String des="";
    private String eventid="";
    private String image;
    private String bg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_create_agenda);
        if (getIntent() != null) {
            tag = getIntent().getStringExtra("tag");
        }
        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        spinner = (Spinner) findViewById(R.id.spinner);
        select_user = findViewById(R.id.textView6);
        imageView6 = findViewById(R.id.imageView6);
        relativeLayout3 = findViewById(R.id.relativeLayout3);
        textView5 = findViewById(R.id.textView5);
        textView6 = findViewById(R.id.textView6);
        textView = findViewById(R.id.textView);
        imageView3 = findViewById(R.id.imageView3);
        edtTitle = findViewById(R.id.edtTitle);
        tvTitle = findViewById(R.id.tvTitle);
        submit = findViewById(R.id.submit);
        toolbar = findViewById(R.id.toolbar);
        imageView5 = findViewById(R.id.imageView5);
        uploadICon = findViewById(R.id.uploadICon);
        ivBackground = findViewById(R.id.ivBackground);
        edtDescription = findViewById(R.id.edtDescription);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imageView4 = findViewById(R.id.imageView4);

        select_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int activity = 1;
                Intent i = new Intent(CreateAgendaActivity.this, SelectuserActivity.class);
                startActivityForResult(i, activity);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tag.equalsIgnoreCase("edit")) {
                    if(edtTitle.getText().toString().isEmpty()){
                        MyToast.display(CreateAgendaActivity.this, "Please Enter Agenda Title");
                    }else if(edtDescription.getText().toString().isEmpty()){
                        MyToast.display(CreateAgendaActivity.this, "Please Enter Description");
                    }else {
                        if (Methods.isOnline(CreateAgendaActivity.this)) {
                            EditAgenda();
                        }
                    }
                } else {
                    if (event_id.isEmpty()) {
                        MyToast.display(CreateAgendaActivity.this, "Please select Event");
                    } else if (edtDescription.getText().toString().isEmpty()) {
                        MyToast.display(CreateAgendaActivity.this, "Please Enter Description");
                    } else if (icon_string.isEmpty()) {
                        MyToast.display(CreateAgendaActivity.this, "Please Select Icon Image");
                    } else if (bg_string.isEmpty()) {
                        MyToast.display(CreateAgendaActivity.this, "Please Select Background Image");
                    } else if (userlist.isEmpty()) {
                        MyToast.display(CreateAgendaActivity.this, "Please Select Users");
                    } else {
                        if (Methods.isOnline(CreateAgendaActivity.this)) {
                            postAgenda();
                        }
                    }
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                event_id = "";
                if (position > 0) {
                    // routeName = parent.getItemAtPosition(position).toString();
                    event_id = agendaResponserArrayList.get(position - 1).getId();
                    Log.e("event_id", "-----------" + event_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                LayoutInflater factory = LayoutInflater.from(v.getContext());
                final View view = factory.inflate(R.layout.privacy, null);
                alertDialog.setView(view);
                final TextView no = (TextView) view.findViewById(R.id.submit);
                final EditText editText = (EditText) view.findViewById(R.id.editText);
                alertDialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        createAgenda(editText.getText().toString());
                        // Write your code here to invoke YES event
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
        if (Methods.isOnline(this)) {
            getAgendaList();
        }
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                icon_image = true;
                getImagePicker().start();
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                background_image = true;
                getImagePicker().start();
            }
        });
        if (tag.equalsIgnoreCase("")) {
            imageView3.setVisibility(View.VISIBLE);
            imageView6.setVisibility(View.VISIBLE);
            textView5.setVisibility(View.VISIBLE);
            textView6.setVisibility(View.VISIBLE);
            textView.setVisibility(View.VISIBLE);
            relativeLayout3.setVisibility(View.VISIBLE);
            edtTitle.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
        } else {
            eventid= getIntent().getStringExtra("event_id");
            title= getIntent().getStringExtra("title");
            des= getIntent().getStringExtra("des");
            image= getIntent().getStringExtra("image");
            bg= getIntent().getStringExtra("bg");
            edtTitle.setText(title);
            edtDescription.setText(des);
            Picasso.get().load(Base_url+image)
                    .placeholder(R.drawable.logo)
                    .into(uploadICon);
            Picasso.get().load(Base_url+bg)
                    .placeholder(R.drawable.logo)
                    .into(ivBackground);
            uploadICon.setVisibility(View.VISIBLE);
            ivBackground.setVisibility(View.VISIBLE);
            imageView6.setVisibility(View.GONE);
            textView6.setVisibility(View.GONE);
            textView5.setVisibility(View.GONE);
            imageView3.setVisibility(View.GONE);
            relativeLayout3.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
            edtTitle.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
         /*   myList = (ArrayList<String>) getIntent().getSerializableExtra("userlist");
            Log.e("list", ">>>>>>>>>>>>>>>>>>>>>" + myList);

            JSONObject obj = null;
            jsonArray = new JSONArray();
            for (int i = 0; i < myList.size(); i++) {
                obj = new JSONObject();
                try {
                    obj.put("user_id", myList.get(i));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                jsonArray.put(obj);
            }
            Log.e("jsonArray", ">>>>>>>>>>>>>>" + jsonArray);
       */
        }
    }

    private void createAgenda(String title) {
        myStateview.showLoading();
        AndroidNetworking.post(agenda_subject_store)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("title", title)
                .addBodyParameter("login_id", myPreferences.getId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                if (Methods.isOnline(CreateAgendaActivity.this)) {
                                    getAgendaList();
                                }
                            } else {
                                myStateview.showContent();
                                MyToast.display(CreateAgendaActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void getAgendaList() {
        if (!agendaList.isEmpty()) {
            agendaList.clear();
        }
        myStateview.showLoading();
        AndroidNetworking.get(unassign_agenda_subject)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                JSONArray jsonArray = response.getJSONObject("data").getJSONArray("subject");
                                agendaList.add("Select Agenda");
                                agendaResponserArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    SelectAgendaResponser countryResponser = new SelectAgendaResponser();
                                    countryResponser.setId(jsonObject.getString("id"));
                                    countryResponser.setTitle(jsonObject.getString("title"));
                                    agendaList.add(jsonObject.getString("title"));
                                    agendaResponserArrayList.add(countryResponser);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateAgendaActivity.this, R.layout.spinner_list, agendaList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner.setAdapter(adapter);

                            } else {
                                myStateview.showContent();
                                MyToast.display(CreateAgendaActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void postAgenda() {
        myStateview.showLoading();
        Log.e("userlist", ">>>>>>>>>>>>>>>>>>" + userlist.replace("[", "").replace("]", ""));
        AndroidNetworking.upload(agenda_store)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addMultipartParameter("event_id", event_id)
                .addMultipartParameter("description", edtDescription.getText().toString())
                .addMultipartParameter("login_id", myPreferences.getId())
                .addMultipartParameter("users", userlist.replace("[", "").replace("]", ""))
                .addMultipartFile("icon_image", iconfile)
                .addMultipartFile("background_image", backgroundfile)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        UserListResponser userListResponser = gson.fromJson(response.toString(), UserListResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                MyToast.display(CreateAgendaActivity.this, response.getString("message"));
                                finish();

                            } else {
                                myStateview.showContent();
                                MyToast.display(CreateAgendaActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private void EditAgenda() {
        myStateview.showLoading();
        Log.e("eventid",">>>>>>>>>>>>>>"+eventid);
        Log.e("eventid",">>>>>>>>>>>>>>"+eventid);
        Log.e("userlist", ">>>>>>>>>>>>>>>>>>" + userlist.replace("[", "").replace("]", ""));
        AndroidNetworking.upload(agenda_update)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addMultipartParameter("event_id", eventid)
                .addMultipartParameter("title", edtTitle.getText().toString())
                .addMultipartParameter("description", edtDescription.getText().toString())
                .addMultipartFile("icon_image", iconfile)
                .addMultipartFile("background_image", backgroundfile)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);
                        Gson gson = new Gson();
                        UserListResponser userListResponser = gson.fromJson(response.toString(), UserListResponser.class);
                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();
                                MyToast.display(CreateAgendaActivity.this, response.getString("message"));
                                finish();

                            } else {
                                myStateview.showContent();
                                MyToast.display(CreateAgendaActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    private ImagePicker getImagePicker() {

        ImagePicker imagePicker = ImagePicker.create(this)
                .language("in") // Set image picker language
                .theme(R.style.ImagePickerTheme)
                .returnMode(false
                        ? ReturnMode.ALL
                        : ReturnMode.NONE) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(false) // set folder mode (false by default)
                .includeVideo(false) // include video (false by default)
                .onlyVideo(false) // include video (false by default)
                .toolbarArrowColor(Color.RED) // set toolbar arrow up color
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarDoneButtonText("DONE"); // done button text
        ImagePickerComponentHolder.getInstance()
                .setImageLoader(new DefaultImageLoader());

        //  if (isSingleMode) {
        imagePicker.single();
     /*   } else {
            imagePicker.multi(); // multi mode (default mode)
        }*/

       /* if (isExclude) {
            imagePicker.exclude(images); // don't show anything on this selected images
        } else {
            imagePicker.origin(images); // original selected images, used in multi mode
        }*/

        return imagePicker.limit(10) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()); // can be full path
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            ArrayList<Image> images = new ArrayList<>();
            Log.e("request", ">>>>>>>>>>>>>>" + requestCode);

            images = (ArrayList<Image>) ImagePicker.getImages(data);
            if (icon_image == true) {
                icon_image = false;
                uploadICon.setVisibility(View.VISIBLE);
                iconfile = new File(images.get(0).getPath());
                icon_string = String.valueOf(iconfile);
                Log.e("iconfile", ">>>>>>>>>>>>>>>>>>>>" + iconfile);
                uploadICon.setImageBitmap(BitmapFactory.decodeFile(images.get(0).getPath()));
            }
            if (background_image == true) {
                background_image = false;
                ivBackground.setVisibility(View.VISIBLE);
                backgroundfile = new File(images.get(0).getPath());
                bg_string = String.valueOf(backgroundfile);
                Log.e("backgroundfile", ">>>>>>>>>>>>>>>>>>>>" + backgroundfile);
                ivBackground.setImageBitmap(BitmapFactory.decodeFile(images.get(0).getPath()));
            }
            Log.e("image", ">>>>>>>>>>>>>>" + images);

            printImages(images);
            return;
        }
        Log.e(">>>>>>>>>>>>>>>>>>>>>>", ">>>>>>>>>>>>>>>>>>>>>>>>iuui");
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(">>>>>>>>>>>>>>>>>>>>>>", ">>>>>>>>>>>>>>>>>>>>>>>>iuui");
        if (requestCode == 1 & data != null) {
            myList = (ArrayList<String>) data.getSerializableExtra("userlist");
            Log.e("list", ">>>>>>>>>>>>>>>>>>>>>" + myList);
            userlist = String.valueOf(myList);
               /* JSONObject obj = null;
                jsonArray = new JSONArray();
                for (int i = 0; i < myList.size(); i++) {
                    obj = new JSONObject();
                    try {
                        obj.put("user_id", myList.get(i));
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    jsonArray.put(obj);
                }
                Log.e("jsonArray",">>>>>>>>>>>>>>"+jsonArray);*/

        }
    }

    private void printImages(List<Image> images) {
        if (images == null) return;

        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0, l = images.size(); i < l; i++) {
            stringBuffer.append(images.get(i).getPath()).append("\n");
        }
        Log.e("image", ">>>>>>>>>>>>>>>>>>>>" + stringBuffer.toString());
 /*       textView.setText(stringBuffer.toString());
        textView.setOnClickListener(v -> ImageViewerActivity.start(MainActivity.this, images));*/
    }

    @Override
    public void onRetryClick() {

    }
}
