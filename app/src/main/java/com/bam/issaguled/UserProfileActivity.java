package com.bam.issaguled;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bam.issaguled.other.Methods;
import com.bam.issaguled.other.MyPreferences;
import com.bam.issaguled.other.MyStateview;
import com.bam.issaguled.other.MyToast;
import com.bam.issaguled.other.ProgressClickListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.login.LoginManager;
import com.twitter.sdk.android.core.TwitterCore;

import org.json.JSONException;
import org.json.JSONObject;

import static com.bam.issaguled.other.API_LIST.get_user_detail;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileActivity extends AppCompatActivity implements ProgressClickListener {
    ImageView camera;
    Button donate;
    Button logout;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    ImageView image;
    ImageView ivLogout;
    private MyPreferences myPreferences;
    private MyStateview myStateview;
    private Toolbar toolbar;
    private TextView tvMyDonation;
    private TextView tvEmail;
    private TextView tvMobileNumber;
    private TextView tvName;
    private TextView tvCity;
    private TextView tvState;
    private TextView tvCountry;
    private TextView tvLocation;
    private AlertDialog dialog;
    private ImageView ivEdit;
    private String name="";
    private String mobile="";
    private String city="";
    private String location="";
    private String state="";
    private String email="";
    private String country="";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);

        myPreferences = new MyPreferences(this);
        myStateview = new MyStateview(this, null);

        tvLocation = findViewById(R.id.tvLocation);
        ivEdit = findViewById(R.id.ivEdit);
        logout = findViewById(R.id.logout);
        tvCity = findViewById(R.id.tvCity);
        tvCountry = findViewById(R.id.tvCountry);
        tvMobileNumber = findViewById(R.id.tvMobileNumber);
        tvName = findViewById(R.id.tvName);
        tvEmail = findViewById(R.id.tvEmail);
        tvMyDonation = findViewById(R.id.tvMyDonation);
        toolbar = findViewById(R.id.toolbar);
        image = findViewById(R.id.image);
        donate = findViewById(R.id.donate);
        tvState = findViewById(R.id.tvState);
        ivLogout = findViewById(R.id.ivLogout);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(UserProfileActivity.this);
                View mView1 = getLayoutInflater().inflate(R.layout.dialog_logout, null);
                TextView TvExit = mView1.findViewById(R.id.TvExit);
                TextView TvClose = mView1.findViewById(R.id.TvClose);
                TvExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if(myPreferences.getLogin_type().equalsIgnoreCase("self")) {
                            myPreferences.clearPreferences();
                            myPreferences.setFirst_time("1");
                        }else if(myPreferences.getLogin_type().equalsIgnoreCase("facebook")){
                            LoginManager.getInstance().logOut();
                            myPreferences.clearPreferences();
                            myPreferences.setFirst_time("1");
                        }else {
                            CookieSyncManager.createInstance(UserProfileActivity.this);
                            CookieManager cookieManager = CookieManager.getInstance();
                            cookieManager.removeSessionCookie();
                            TwitterCore.getInstance().getSessionManager().clearActiveSession();
                            myPreferences.clearPreferences();
                            myPreferences.setFirst_time("1");
                        }
                        MyToast.display(UserProfileActivity.this, "Logged Out");
                        startActivity(new Intent(UserProfileActivity.this, SignInActivity.class));
                        finishAffinity();
                    }
                });
                mBuilder.setView(mView1);
                dialog = mBuilder.create();
                dialog.show();
            }
        });

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(UserProfileActivity.this);
                View mView1 = getLayoutInflater().inflate(R.layout.dialog_logout, null);
                TextView TvExit = mView1.findViewById(R.id.TvExit);
                TextView TvClose = mView1.findViewById(R.id.TvClose);
                TvExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if(myPreferences.getLogin_type().equalsIgnoreCase("self")) {
                            myPreferences.clearPreferences();
                            myPreferences.setFirst_time("1");
                        }else if(myPreferences.getLogin_type().equalsIgnoreCase("facebook")){
                            LoginManager.getInstance().logOut();
                            myPreferences.clearPreferences();
                            myPreferences.setFirst_time("1");
                        }else {
                            CookieSyncManager.createInstance(UserProfileActivity.this);
                            CookieManager cookieManager = CookieManager.getInstance();
                            cookieManager.removeSessionCookie();
                            TwitterCore.getInstance().getSessionManager().clearActiveSession();
                            myPreferences.clearPreferences();
                            myPreferences.setFirst_time("1");
                        }
                        MyToast.display(UserProfileActivity.this, "Logged Out");
                        startActivity(new Intent(UserProfileActivity.this, SignInActivity.class));
                        finishAffinity();
                    }
                });
                mBuilder.setView(mView1);
                dialog = mBuilder.create();
                dialog.show();
            }
        });

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserProfileActivity.this,EditProfileActivity.class)
                .putExtra("name",name)
                .putExtra("email",email)
                .putExtra("mobile",mobile)
                .putExtra("city",city)
                .putExtra("country",country)
                .putExtra("location",location)
                .putExtra("state",state)
                .putExtra("tag","user")
                );
            }
        });

        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, DonateActivity.class);
                intent.putExtra("event_id","");
                startActivity(intent);
            }
        });

        if (Methods.isOnline(this)) {
            getDetails();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            //TODO: action
            imageUri = data.getData();
            image.setImageURI(imageUri);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDetails();
    }

    private void getDetails() {
        myStateview.showLoading();
        AndroidNetworking.post(get_user_detail)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer" + myPreferences.getToken())
                .addBodyParameter("user_id", myPreferences.getId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", ">>>>>>>>>>>" + response);

                        try {
                            if (response.getString("success").equalsIgnoreCase("200")) {
                                myStateview.showContent();

                                name=response.getJSONObject("data").getString("name");
                                email= response.getJSONObject("data").getString("email");
                                mobile= response.getJSONObject("data").getString("mobile_number");
                                city=response.getJSONObject("data").getString("city");
                                country=response.getJSONObject("data").getString("country");
                                location=response.getJSONObject("data").getString("location");
                                state=response.getJSONObject("data").getString("state");

                                tvMyDonation.setText("$"+response.getJSONObject("data").getString("my_donation"));
                                tvName.setText("Name : "+response.getJSONObject("data").getString("name"));
                                tvEmail.setText("Email : "+response.getJSONObject("data").getString("email"));
                                tvMobileNumber.setText("Mobile Number : "+response.getJSONObject("data").getString("mobile_number"));
                                tvCity.setText("City : "+response.getJSONObject("data").getString("city"));
                                tvCountry.setText("Country : "+response.getJSONObject("data").getString("country"));
                                tvLocation.setText("Location : "+response.getJSONObject("data").getString("location"));
                                tvState.setText("State : "+response.getJSONObject("data").getString("state"));

                            } else {
                                myStateview.showContent();
                                MyToast.display(UserProfileActivity.this, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        myStateview.showRetry();
                        Log.e("anError", ">>>>>>>>>>>>>>>" + anError.getErrorDetail());

                    }
                });
    }

    @Override
    public void onRetryClick() {
        if (Methods.isOnline(this)) {
            getDetails();
        }
    }
}
